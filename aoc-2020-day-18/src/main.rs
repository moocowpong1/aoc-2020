use std::env;
use std::fs;
use std::error::Error;
use std::time::Instant;

fn main() -> Result<(), Box<dyn Error>> {
    let input = fs::read_to_string(env::args().nth(1).unwrap_or(String::from("puzzle_input.txt")))?;
    let input2 = input.clone();
    let part1_start = Instant::now();
    part1(input);
    let part1_duration = part1_start.elapsed();
    println!("Part 1 time: {:?}\n", part1_duration);

    let part2_start = Instant::now();
    part2(input2);
    let part2_duration = part2_start.elapsed();
    println!("Part 2 time: {:?}\n", part2_duration);

    Ok(())
}

enum Op {
    Plus,
    Times,
}

fn calc(n1: u64, op: Op, n2: u64) -> u64 {
    match op {
        Op::Plus => n1 + n2,
        Op::Times => n1 * n2,
    }
}

fn part1(input: String) {
    let mut answers: Vec<u64> = Vec::new();
    for line in input.split('\n') {
        let mut stack: Vec<(u64, Op)> = vec![(0u64, Op::Plus)];      // initialize the stack with a trivial computation, so that we have a starting point
        let mut num = 0u64;
        let mut bytes_iter = line.bytes();
        loop {

           match bytes_iter.next() {
                None => {                                            // At the end of a line, pop the pending operation and finish.
                    let (prev_num, prev_op) = stack.pop().unwrap();
                    answers.push(calc(prev_num, prev_op, num)); 
                    break;
                },               
                Some(b' ') => (),                                    // Ignore spaces
                Some(d) if (b'0' <= d) && (d <= b'9') => {           // If we see a digit, tack it onto the current num.
                    num *= 10; 
                    num += (d - b'0') as u64; 
                }   
                Some(op) if (b'+' == op || b'*' == op) => {
                    let (prev_num, prev_op) = stack.pop().unwrap();  // If we see an operator, we know we're done with the previous calculation;
                    num = calc(prev_num, prev_op, num);              // so compute (b/c left-associative) and then push the result with the new operation.
                    let op = match op { b'+' => Op::Plus, b'*' => Op::Times, _ => panic!("Unreachable."), };
                    stack.push((num, op));
                    num = 0;
                }
                Some(b'(') => {                                      // An open paren pushes a new trivial computation onto the stack.
                    stack.push((0u64, Op::Plus));                    // Parens can only appear after operators or other parens, so it's okay for them to stomp
                    num = 0u64;                                      // the current num.
                }
                Some(b')') => {                                      // A close paren pops the stack and performs the pending computation. Not pushing
                    let (prev_num, prev_op) = stack.pop().unwrap();  // anything else returns us to the previous computation.
                    num = calc(prev_num, prev_op, num);
                }
                _ => panic!("Unexpected symbol."),
            }
        }
    }

    println!("Sum of all answers: {}", answers.iter().sum::<u64>());
}

// Represents the operation x |-> to_multiply * (to_add + x)
// (Note the strange precedence rules.)
#[derive(Clone, Copy)]
struct PendingOperation {
    to_multiply: u64,
    to_add: u64,
}

impl PendingOperation {
    const TRIVIAL: PendingOperation = PendingOperation { to_multiply: 1u64, to_add: 0u64 };

    fn eval(self, num: u64) -> u64 {
        self.to_multiply * (self.to_add + num)
    }

    fn combine_additive(self, num: u64) -> PendingOperation { 
        PendingOperation { to_add: self.to_add + num, ..self }
    }
}

fn part2(input: String) {
    let mut answers: Vec<u64> = Vec::new();
    for line in input.split('\n') {
        let mut stack: Vec<PendingOperation> = vec![PendingOperation::TRIVIAL]; // initialize the stack with a trivial computation, so that we have a starting point
        let mut num = 0u64;
        let mut bytes_iter = line.bytes();
        loop {

           match bytes_iter.next() {
                None => {                                            // At the end of a line, pop the pending operation and finish.
                    let pend = stack.pop().unwrap();
                    answers.push(pend.eval(num));
                    break;
                },               
                Some(b' ') => (),                                    // Ignore spaces
                Some(d) if (b'0' <= d) && (d <= b'9') => {           // If we see a digit, tack it onto the current num.
                    num *= 10; 
                    num += (d - b'0') as u64; 
                }   
                Some(b'*') => {
                    let pend = stack.pop().unwrap();                  // If we see a times, we compute the whole pending operation and set up a new
                    let result = pend.eval(num);                      // one to multiply by the result
                    stack.push(PendingOperation { to_multiply: result, to_add: 0});
                    num = 0;
                }
                Some(b'+') => {
                    let pend = stack.pop().unwrap();                  // If we see a plus, we don't consume the whole pending operation, just accumulate
                    let new_pend = pend.combine_additive(num);        // it onto the additive part of the pending operation due to higher precedence.
                    stack.push(new_pend);
                    num = 0;
                }
                Some(b'(') => {                                      // An open paren pushes a new trivial computation onto the stack.
                    stack.push(PendingOperation::TRIVIAL);           // Parens can only appear after operators or other parens, so it's okay for them to stomp
                    num = 0u64;                                      // the current num.
                }
                Some(b')') => {                                      // A close paren pops the stack and performs the pending computation. Not pushing
                    let pend = stack.pop().unwrap();                 // anything else returns us to the previous computation.
                    num = pend.eval(num);
                }
                _ => panic!("Unexpected symbol."),
            }
        }
    }

    println!("Sum of all answers: {}", answers.iter().sum::<u64>());
}
