use std::io;

fn main() {
    let mut joltages: Vec<usize> = Vec::new();
    let mut max_joltage = 0usize;

    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read from stdin");
        let trimmed = input.trim();
        if trimmed.len() == 0 { break; }
        let n = trimmed.parse().expect("Failed to read integer");
        joltages.push(n);
        if n > max_joltage { max_joltage = n }
    }

    let final_joltage = max_joltage + 3;
    joltages.sort();
    joltages.push(final_joltage);

    let mut prev_jolt = 0;
    let mut diffs_1 = 0;
    let mut diffs_3 = 0;
    for jolt in joltages.iter() {
        match jolt - prev_jolt {
            1 => diffs_1 += 1,
            2 => (),
            3 => diffs_3 += 1,
            _ => panic!("Invalid joltage gap: {} => {}", prev_jolt, jolt),
        }
        prev_jolt = *jolt;
    }

    //Compute the number of possible adapter arrangements by memoizing.
    //num_arrangements[j] stores the number of arrangements of adapters which starts with a joltage j adapter and reaches final_joltage.
    let mut num_arrangements: Vec<u64> = vec![0; final_joltage + 1];
    num_arrangements[final_joltage as usize] = 1;

    for jolt in joltages.iter().rev().skip(1) {     // start with the highest joltage adapter and work down, ignoring final_joltage.
        //the next adapter in the arrangement can be 1, 2, or 3 jolts higher; each arrangement beginning with one of these yields a distinct adapter arrangement
        //because final_joltage >= jolt + 3, this doesn't go out of bounds
        num_arrangements[*jolt] = num_arrangements[*jolt+1] + num_arrangements[*jolt+2] + num_arrangements[*jolt+3];
    }
    //the wall joltage of 0 wasn't in our list, so we do this final iteration here
    num_arrangements[0] = num_arrangements[1] + num_arrangements[2] + num_arrangements[3];

    println!("1-jolt differences: {}\n3-jolt differences: {}\nProduct: {}", diffs_1, diffs_3, diffs_1*diffs_3);
    println!("Number of adapter arrangements starting at 0: {}", num_arrangements[0]);
}
