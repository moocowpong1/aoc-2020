use std::env;
use std::fs;
use std::error::Error;
use std::time::Instant;

fn main() -> Result<(), Box<dyn Error>> {
    let input: String;
    if let Some(arg1) = env::args().nth(1) {
        input = fs::read_to_string(arg1)?;
    } else {
        input = fs::read_to_string("puzzle_input.txt")?;
    }
    let input2 = input.clone();
    let part1_start = Instant::now();
    part1(input);
    let part1_duration = part1_start.elapsed();
    println!("Part 1 time: {:?}\n", part1_duration);

    let part2_start = Instant::now();
    part2(input2);
    let part2_duration = part2_start.elapsed();
    println!("Part 2 time: {:?}\n", part2_duration);

    Ok(())
}

struct CubeGrid {
    // We will maintain the invariant that the edgemost cubes in the grid are inactive, expanding if necessary to do so.
    xdim: usize,
    ydim: usize,
    zdim: usize,
    cubes: Vec<bool>,   // cube (x,y,z) is stored at index with z*ydim*xdim + y*xdim + x
    neighbors: Vec<u8>, // only used temporarily during the update process; holding onto it keeps us from having to reallocate it.
}

impl CubeGrid {
    fn new(xdim: usize, ydim: usize, zdim: usize, cubes: Vec<bool>) -> Self {
        let size = xdim*ydim*zdim;
        let neighbors = vec!(0u8; size);
        let mut cube_grid = CubeGrid { xdim, ydim, zdim, cubes, neighbors };

        // Expand in all directions to make sure the inactive edges invariant holds.
        cube_grid.expand_x();
        cube_grid.expand_y();
        cube_grid.expand_z();

        cube_grid
    }

    // triples the Z dimension of the cube grid, placing the old grid at the center
    fn expand_z(&mut self) {
        let size = (self.xdim)*(self.ydim)*(self.zdim);
        let mut new_cubes = vec!(false; size);
        new_cubes.append(&mut self.cubes);
        new_cubes.resize(3*size, false);
        self.cubes = new_cubes;
        self.neighbors = vec!(0; 3*size);    // okay to trample neighbors here
        self.zdim *= 3;
    } 

    // triples the Y dimension
    fn expand_y(&mut self) {
        let zdim = self.zdim;
        let plane_area = (self.xdim)*(self.ydim);
        let mut new_cubes = Vec::new();
        let mut cubes_iter = self.cubes.iter();
        for _ in 0..zdim {
            new_cubes.resize(new_cubes.len() + plane_area, false);
            for cube in (&mut cubes_iter).take(plane_area) {
                new_cubes.push(*cube);
            }
            new_cubes.resize(new_cubes.len() + plane_area, false);
        }
        self.cubes = new_cubes;
        self.neighbors = vec!(0; 3*zdim*plane_area);
        self.ydim *= 3;
    }

    // triples the X dimension
    fn expand_x(&mut self) {
        let num_rows = (self.zdim)*(self.ydim);
        let xdim = self.xdim;
        let mut new_cubes = Vec::new();
        let mut cubes_iter = self.cubes.iter();
        for _ in 0..num_rows {
            new_cubes.resize(new_cubes.len() + xdim, false);
            for cube in (&mut cubes_iter).take(xdim) {
                new_cubes.push(*cube);
            }
            new_cubes.resize(new_cubes.len() + xdim, false);
        }

        self.cubes = new_cubes;
        self.neighbors = vec!(0; 3*num_rows*xdim);
        self.xdim *= 3;
    }

    fn cycle(&mut self) {
        let xdim = self.xdim;
        let ydim = self.ydim;
        let zdim = self.zdim;
        let mut needs_expand_x = false;
        let mut needs_expand_y = false;
        let mut needs_expand_z = false;
        self.update_neighbors();
        for (i, (cube, neighbor_count)) in self.cubes.iter_mut().zip(self.neighbors.iter()).enumerate() {
            match *neighbor_count {
                3 => {
                    *cube = true;        // cubes with three active neighbors become active. These force us to check for expansion.
                    let x = i % xdim;
                    let y = ((i - x)/xdim) % ydim;
                    let z = (i - x - y*xdim)/(xdim*ydim);
                    if z == 0 || z == zdim - 1 { needs_expand_z = true; }
                    if y == 0 || y == ydim - 1 { needs_expand_y = true; }
                    if x == 0 || x == xdim - 1 { needs_expand_x = true; }
                }
                2 => (),                // cubes with two active neighbors are unchanged
                _ => *cube = false,     // all others become inactive
            }
        }
        if needs_expand_x { self.expand_x() }
        if needs_expand_y { self.expand_y() }
        if needs_expand_z { self.expand_z() }
    }
    
    fn update_neighbors(&mut self) {
        let mut offsets = Vec::new();
        for dz in (-1isize)..=(1isize) {
            for dy in (-1isize)..=(1isize) {
                for dx in (-1isize)..=(1isize) {
                    if dx == 0 && dy == 0 && dz == 0 { continue }
                    offsets.push(dx + dy*(self.xdim as isize) + dz*((self.xdim*self.ydim) as isize));
                }
            }
        }

        let cubes = &self.cubes;
        let len = cubes.len() as isize;

        // We handle boundary conditions in a strange way here - by ignoring them! We use fixed offsets in the flat array to calculate
        // neighbors. This gives very strange boundary conditions in 3D, but because we are guaranteed to have inactive
        // cubes along the edges of our region, it gives the same result as if the grid were embedded in an infinite grid of
        // inactive cubes as required.
        for (i, neigh) in self.neighbors.iter_mut().enumerate() {
            *neigh = offsets.iter().map(|off| { let j = i as isize + off; if j>=0 && j<len {cubes[j as usize]} else {false} }).filter(|b| *b).count() as u8;
        }
    }

    fn num_active_cubes(&self) -> usize {
        self.cubes.iter().filter(|b| **b).count()
    }
}

fn part1(input: String) {
    let mut cubes = Vec::new();
    let mut input_width:Option<usize> = None;
    for line in input.split('\n') {
        let bytes = line.as_bytes();
        if input_width.is_none() { input_width = Some(bytes.len()) }
        for byte in bytes {
            cubes.push(*byte == b'#');   // interpret # as an active cube and all other inputs as inactive cubes.
        }
    }
    let xdim = input_width.unwrap();
    let ydim = cubes.len()/xdim; // cubes.len() is an exact multiple of xdim for well-formed inputs.
    
    let mut cube_grid = CubeGrid::new(xdim, ydim, 1, cubes);

    for _ in 0..6 {
        cube_grid.cycle();
    }

    println!("Number of cubes active after six cycles: {}", cube_grid.num_active_cubes());

}

// 4D version for part 2
struct CubeGrid4 {
    // We will maintain the invariant that the edgemost cubes in the grid are inactive, expanding if necessary to do so.
    xdim: usize,
    ydim: usize,
    zdim: usize,
    wdim: usize,
    cubes: Vec<bool>,   // cube (x,y,z, w) is stored at index with w*zdim*ydim*xdim + z*ydim*xdim + y*xdim + x
    neighbors: Vec<u8>, // only used temporarily during the update process; holding onto it keeps us from having to reallocate it.
}

impl CubeGrid4 {
    fn new(xdim: usize, ydim: usize, zdim: usize, wdim: usize, cubes: Vec<bool>) -> Self {
        let size = xdim*ydim*zdim*wdim;
        let neighbors = vec!(0u8; size);
        let mut cube_grid = CubeGrid4 { xdim, ydim, zdim, wdim, cubes, neighbors };

        // Expand in all directions to make sure the inactive edges invariant holds.
        cube_grid.expand_x();
        cube_grid.expand_y();
        cube_grid.expand_z();
        cube_grid.expand_w();

        cube_grid
    }

    // triples the W dimension of the cube grid, placing the old grid at the center
    fn expand_w(&mut self) {
        let size = (self.xdim)*(self.ydim)*(self.zdim)*(self.wdim);
        let mut new_cubes = vec!(false; size);
        new_cubes.append(&mut self.cubes);
        new_cubes.resize(3*size, false);
        self.cubes = new_cubes;
        self.neighbors = vec!(0; 3*size);    // okay to trample neighbors here
        self.wdim *= 3;
    } 

    // triples the Z dimension
    fn expand_z(&mut self) {
        let wdim = self.wdim;
        let xyzdim = (self.xdim)*(self.ydim)*(self.zdim);
        let mut new_cubes = Vec::new();
        let mut cubes_iter = self.cubes.iter();
        for _ in 0..wdim {
            new_cubes.resize(new_cubes.len() + xyzdim, false);
            for cube in (&mut cubes_iter).take(xyzdim) {
                new_cubes.push(*cube);
            }
            new_cubes.resize(new_cubes.len() + xyzdim, false);
        }
        self.cubes = new_cubes;
        self.neighbors = vec!(0; 3*wdim*xyzdim);
        self.zdim *= 3;
    }

    // triples the Y dimension
    fn expand_y(&mut self) {
        let zwdim = (self.zdim)*(self.wdim);
        let xydim = (self.xdim)*(self.ydim);
        let mut new_cubes = Vec::new();
        let mut cubes_iter = self.cubes.iter();
        for _ in 0..zwdim {
            new_cubes.resize(new_cubes.len() + xydim, false);
            for cube in (&mut cubes_iter).take(xydim) {
                new_cubes.push(*cube);
            }
            new_cubes.resize(new_cubes.len() + xydim, false);
        }
        self.cubes = new_cubes;
        self.neighbors = vec!(0; 3*zwdim*xydim);
        self.ydim *= 3;
    }

    // triples the X dimension
    fn expand_x(&mut self) {
        let yzwdim = (self.ydim)*(self.zdim)*(self.wdim);
        let xdim = self.xdim;
        let mut new_cubes = Vec::new();
        let mut cubes_iter = self.cubes.iter();
        for _ in 0..yzwdim {
            new_cubes.resize(new_cubes.len() + xdim, false);
            for cube in (&mut cubes_iter).take(xdim) {
                new_cubes.push(*cube);
            }
            new_cubes.resize(new_cubes.len() + xdim, false);
        }

        self.cubes = new_cubes;
        self.neighbors = vec!(0; 3*yzwdim*xdim);
        self.xdim *= 3;
    }

    fn cycle(&mut self) {
        let xdim = self.xdim;
        let ydim = self.ydim;
        let zdim = self.zdim;
        let wdim = self.wdim;
        let mut needs_expand_x = false;
        let mut needs_expand_y = false;
        let mut needs_expand_z = false;
        let mut needs_expand_w = false;
        self.update_neighbors();
        for (i, (cube, neighbor_count)) in self.cubes.iter_mut().zip(self.neighbors.iter()).enumerate() {
            match *neighbor_count {
                3 => {
                    *cube = true;        // cubes with three active neighbors become active. These force us to check for expansion.
                    let x = i % xdim;
                    let y = ((i - x)/xdim) % ydim;
                    let z = ((i - x - y*xdim)/(xdim*ydim)) % zdim;
                    let w = (i - x - y*xdim - z*xdim*ydim)/(xdim*ydim*zdim);
                    if w == 0 || w == wdim - 1 { needs_expand_w = true; }
                    if z == 0 || z == zdim - 1 { needs_expand_z = true; }
                    if y == 0 || y == ydim - 1 { needs_expand_y = true; }
                    if x == 0 || x == xdim - 1 { needs_expand_x = true; }
                }
                2 => (),                // cubes with two active neighbors are unchanged
                _ => *cube = false,     // all others become inactive
            }
        }
        if needs_expand_x { self.expand_x() }
        if needs_expand_y { self.expand_y() }
        if needs_expand_z { self.expand_z() }
        if needs_expand_w { self.expand_w() }
    }
    
    fn update_neighbors(&mut self) {
        let mut offsets = Vec::new();
        for dw in (-1isize)..=(1isize) {
            for dz in (-1isize)..=(1isize) {
                for dy in (-1isize)..=(1isize) {
                    for dx in (-1isize)..=(1isize) {
                        if dx == 0 && dy == 0 && dz == 0 && dw == 0 { continue }
                        offsets.push(dx + dy*(self.xdim as isize) + dz*((self.xdim*self.ydim) as isize) 
                                    + dw*((self.xdim*self.ydim*self.zdim) as isize));
                    }
                }
            }
        }
        

        let cubes = &self.cubes;
        let len = cubes.len() as isize;

        // We handle boundary conditions in a strange way here - by ignoring them! We use fixed offsets in the flat array to calculate
        // neighbors. This gives very strange boundary conditions in 4D, but because we are guaranteed to have inactive
        // cubes along the edges of our region, it gives the same result as if the grid were embedded in an infinite grid of
        // inactive cubes as required.
        for (i, neigh) in self.neighbors.iter_mut().enumerate() {
            *neigh = offsets.iter().map(|off| { let j = i as isize + off; if j>=0 && j<len {cubes[j as usize]} else {false} }).filter(|b| *b).count() as u8;
        }
    }

    fn num_active_cubes(&self) -> usize {
        self.cubes.iter().filter(|b| **b).count()
    }
}

fn part2(input: String) {
    let mut cubes = Vec::new();
    let mut input_width:Option<usize> = None;
    for line in input.split('\n') {
        let bytes = line.as_bytes();
        if input_width.is_none() { input_width = Some(bytes.len()) }
        for byte in bytes {
            cubes.push(*byte == b'#');   // interpret # as an active cube and all other inputs as inactive cubes.
        }
    }
    let xdim = input_width.unwrap();
    let ydim = cubes.len()/xdim; // cubes.len() is an exact multiple of xdim for well-formed inputs.
    
    let mut cube_grid = CubeGrid4::new(xdim, ydim, 1, 1, cubes);

    for _ in 0..6 {
        cube_grid.cycle();
    }

    println!("Number of cubes active after six cycles: {}", cube_grid.num_active_cubes());

}
