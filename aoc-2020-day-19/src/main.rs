use std::env;
use std::fs;
use std::error::Error;
use std::time::Instant;
use std::collections::BTreeMap;
use std::collections::BTreeSet;

fn main() -> Result<(), Box<dyn Error>> {
    let input = fs::read_to_string(env::args().nth(1).unwrap_or(String::from("puzzle_input.txt")))?;
    let input2 = input.clone();
    let part1_start = Instant::now();
    part1(input);
    let part1_duration = part1_start.elapsed();
    println!("Part 1 time: {:?}\n", part1_duration);

    let part2_start = Instant::now();
    part2(input2);
    let part2_duration = part2_start.elapsed();
    println!("Part 2 time: {:?}\n", part2_duration);

    Ok(())
}

enum Rule {
    Lit(u8),
    Rec(Vec<Vec<usize>>),
}

fn part1(input: String) {
    let mut sections = input.split("\n\n");
    let rules_sec = sections.next().unwrap();
    let mut rules = BTreeMap::new();
    for line in rules_sec.lines() {
        let mut parts = line.split(':');
        let rule_num: usize = parts.next().unwrap().parse().unwrap();
        let rule_spec = parts.next().unwrap().trim();
        let rule = match rule_spec.as_bytes()[0] {
            b'"' => Rule::Lit(rule_spec.as_bytes()[1]),
            _ => {
                let mut alternatives = Vec::new();
                for alt in rule_spec.split('|') {
                    let mut sequence = Vec::new();
                    for num in alt.trim().split(' ') {
                        sequence.push(num.parse::<usize>().unwrap());
                    }
                    alternatives.push(sequence);
                }
                Rule::Rec(alternatives)
            }
        };
        rules.insert(rule_num, rule);
    }

    let messages = sections.next().unwrap();
    let valid_messages = messages.lines().filter(|message| matches_rule(0, &rules, message.as_bytes())).count();

    println!("Number of valid messages: {}", valid_messages);
}

fn matches_rule(rule_num: usize, rules: &BTreeMap<usize, Rule>, message: &[u8]) -> bool {
    partial_matches(rule_num, rules, message).contains(&0)
}

// Returns a BTreeSet containing a number if it is possible that it is the length of the remaining portion of message after
// finding a prefix matching rule rule_num.
fn partial_matches(rule_num: usize, rules: &BTreeMap<usize, Rule>, message: &[u8]) -> BTreeSet<usize> {
    let len = message.len();
    match rules.get(&rule_num).unwrap() {
        Rule::Lit(b) => if len > 0 && message[0] == *b { vec![len - 1].into_iter().collect() } else { BTreeSet::new() }
        Rule::Rec(alternatives) => {
            alternatives.iter().flat_map(|sequence| sequence_matches(sequence, rules, message).into_iter() ).collect()
        },
    }
}

fn sequence_matches(sequence: &[usize], rules: &BTreeMap<usize, Rule>, message: &[u8]) -> BTreeSet<usize> {
    let len = message.len();
    if sequence.is_empty() { return vec![len].into_iter().collect() }
    partial_matches(sequence[0], rules, message).iter().flat_map(|remainder_len| {
        sequence_matches(&sequence[1..], rules, &message[(len - remainder_len)..])
    }).collect()
}

fn part2(input: String) {
    let mut sections = input.split("\n\n");
    let rules_sec = sections.next().unwrap();
    let mut rules = BTreeMap::new();
    for line in rules_sec.lines() {
        let mut parts = line.split(':');
        let rule_num: usize = parts.next().unwrap().parse().unwrap();
        let rule_spec = parts.next().unwrap().trim();
        let rule = match rule_spec.as_bytes()[0] {
            b'"' => Rule::Lit(rule_spec.as_bytes()[1]),
            _ => {
                let mut alternatives = Vec::new();
                for alt in rule_spec.split('|') {
                    let mut sequence = Vec::new();
                    for num in alt.trim().split(' ') {
                        sequence.push(num.parse::<usize>().unwrap());
                    }
                    alternatives.push(sequence);
                }
                Rule::Rec(alternatives)
            }
        };
        rules.insert(rule_num, rule);
    }

    rules.insert(8, Rule::Rec(vec![vec![42], vec![42, 8]]));
    rules.insert(11, Rule::Rec(vec![vec![42, 31], vec![42, 11, 31]]));

    let messages = sections.next().unwrap();
    let valid_messages = messages.lines().filter(|message| matches_rule(0, &rules, message.as_bytes())).count();

    println!("Number of valid messages: {}", valid_messages);
}