#[macro_use]
extern crate lazy_static;

use std::env;
use std::fs;
use std::error::Error;
use std::time::Instant;
use regex::Regex;

fn main() -> Result<(), Box<dyn Error>> {
    let input: String;
    if let Some(arg1) = env::args().nth(1) {
        input = fs::read_to_string(arg1)?;
    } else {
        input = fs::read_to_string("puzzle_input.txt")?;
    }
    let input2 = input.clone();
    let part1_start = Instant::now();
    part1(input);
    let part1_duration = part1_start.elapsed();
    println!("Part 1 time: {:?}\n", part1_duration);

    let part2_start = Instant::now();
    part2(input2);
    let part2_duration = part2_start.elapsed();
    println!("Part 2 time: {:?}\n", part2_duration);

    Ok(())
}

lazy_static! {
    static ref RE_FIELDSPEC:Regex = Regex::new(r"([A-Za-z ]+): (\d+)-(\d+) or (\d+)-(\d+)").unwrap();
}

struct FieldSpec {
    name: String,
    range1: (u64, u64),
    range2: (u64, u64),
}

fn matches_field_spec(num: u64, spec: &FieldSpec) -> bool {
    let (lower1, upper1) = spec.range1;
    let (lower2, upper2) = spec.range2;
    ((lower1 <= num) && (num <= upper1)) || ((lower2 <= num) && (num <= upper2))
}

fn matches_any_field_spec(num: u64, specs: &[FieldSpec]) -> bool {
    specs.iter().any(|spec| { matches_field_spec(num, spec) })
}

fn part1(input: String) {

    let mut field_specs:Vec<FieldSpec> = Vec::new();

    let mut sections = input.split("\n\n");

    for line in sections.next().unwrap().split('\n') {
        let caps = RE_FIELDSPEC.captures(line).expect("Failed to match line");
        let name = String::from(&caps[1]);
        let range1 = (caps[2].parse().unwrap(), caps[3].parse().unwrap());
        let range2 = (caps[4].parse().unwrap(), caps[5].parse().unwrap());
        field_specs.push(FieldSpec { name, range1, range2 });
    }
    sections.next(); // skip "your ticket"

    let bad_values:Vec<u64> = sections.next().unwrap().split(|c| { c == '\n' || c == ',' }).skip(1)         //skip "nearby tickets:"
                                        .map(|digits| digits.parse::<u64>().unwrap())
                                        .filter(|value| !matches_any_field_spec(*value, &field_specs)).collect();

    println!("Sum of all values not matching any field spec: {}", bad_values.iter().sum::<u64>());
    


}

fn part2(input: String) {

    let mut field_specs:Vec<FieldSpec> = Vec::new();
    let mut sections = input.split("\n\n");

    for line in sections.next().unwrap().split('\n') {
        let caps = RE_FIELDSPEC.captures(line).expect("Failed to match line");
        let name = String::from(&caps[1]);
        let range1 = (caps[2].parse().unwrap(), caps[3].parse().unwrap());
        let range2 = (caps[4].parse().unwrap(), caps[5].parse().unwrap());
        field_specs.push(FieldSpec { name, range1, range2 });
    }

    let your_ticket: Vec<u64> = sections.next().unwrap().split('\n').nth(1).unwrap().split(',').map(|digits| digits.parse::<u64>().unwrap()).collect();

    let valid_tickets: Vec<Vec<u64>> = sections.next().unwrap().split('\n').skip(1)         //skip "nearby tickets:"
                                        .filter_map(|line| { 
                                            let ticket:Vec<u64> = line.split(',').map(|digits| digits.parse::<u64>().unwrap()).collect();
                                            if ticket.iter().all(|n| matches_any_field_spec(*n, &field_specs)) {
                                                Some(ticket)
                                            } else {
                                                None
                                            }
                                        }).collect();
    
    let num_fields = field_specs.len();
    debug_assert!(num_fields == valid_tickets[0].len());    // double check that the number of fields makes sense

    let mut field_compatibility_matrix: Vec<Vec<bool>> = vec!(vec!(true; num_fields); num_fields);

    for ticket in &valid_tickets {
        for (i, spec) in field_specs.iter().enumerate() {
            for (j, value) in ticket.iter().enumerate() {
                field_compatibility_matrix[i][j] = field_compatibility_matrix[i][j] && matches_field_spec(*value, spec);
            }
        }
    }

    let mut field_indices:Vec<(String, usize)> = Vec::new();
    let mut matched_fields = 0usize;

    while matched_fields < num_fields {
        let mut matching_name: Option<String> = None;
        let mut matching_index: usize = 0;
        for (i, row) in field_compatibility_matrix.iter().enumerate() {
            if let Some(j) = find_exactly_one(row) {
                matching_name = Some(field_specs[i].name.clone());
                matching_index = j;
                break;
            }
        }
        if matching_name.is_none() { panic!("No sufficiently constrained field found") }
        field_indices.push((matching_name.unwrap(), matching_index));
        matched_fields += 1;
        field_compatibility_matrix.iter_mut().for_each(|row| row[matching_index] = false);  // no other field can match this place, so zero out the corresponding column
    }

    #[cfg(debug_assertions)]
    println!("Indices for given fields:");
    #[cfg(debug_assertions)]
    for (name, index) in &field_indices {
        println!("{}: {}", name, index);
    }

    let departure_indices:Vec<usize> = field_indices.iter().filter_map(|(name, index)| if name.len() >= 9 && &name[0..9] == "departure" { Some(*index) } else { None }).collect();

    println!("Product of all \"departure\" fields on your ticket: {}", departure_indices.iter().map(|index| your_ticket[*index]).product::<u64>());

}

fn find_exactly_one(vec: &[bool]) -> Option<usize> {
    let mut result: Option<usize> = None;
    for (i,b) in vec.iter().enumerate() {
        if *b && result.replace(i).is_some() { return None }
    }
    result
}
