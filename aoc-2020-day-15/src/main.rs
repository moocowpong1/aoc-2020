use std::env;
use std::fs;
use std::error::Error;
use std::time::Instant;
use std::collections::HashMap;

fn main() -> Result<(), Box<dyn Error>> {
    let input: String;
    if let Some(arg1) = env::args().nth(1) {
        input = fs::read_to_string(arg1)?;
    } else {
        input = fs::read_to_string("puzzle_input.txt")?;
    }
    let input2 = input.clone();
    let part1_start = Instant::now();
    part1(input);
    let part1_duration = part1_start.elapsed();
    println!("Part 1 time: {:?}\n", part1_duration);

    let part2_start = Instant::now();
    part2(input2);
    let part2_duration = part2_start.elapsed();
    println!("Part 2 time: {:?}\n", part2_duration);

    Ok(())
}

fn part1(input: String) {
    let mut nums: Vec<usize> = Vec::new();
    let mut count = 0;
    for line in input.split(',') {
        let n = line.parse().expect("Parse error");
        nums.insert(0, n);
        count += 1;
    }
    while count < 2020 {
        let mut iter = nums.iter();
        let prev_num = iter.next().unwrap();
        if let Some((i, _)) = iter.enumerate().find(|(_, n)| { *n == prev_num }) {
            nums.insert(0, i + 1);
        } else {
            nums.insert(0, 0);
        }
        count += 1;
    }
    assert!(nums.len() == 2020);
    println!("2020th number spoken: {}", nums[0]);
}

fn part2(input: String) {
    let mut count = 0;
    let mut last_spoken:HashMap<u32, u32> = HashMap::new(); //the count at which a given number was last spoken, except prev_num
    let mut prev_num = 0;
    for line in input.split(',') {
        let n = line.parse().expect("Parse error");
        if count != 0 { last_spoken.insert(prev_num, count - 1); }
        prev_num = n;
        count += 1;
    }
    while count < 30000000 {
        let get_last = last_spoken.get(&prev_num);
        let n;
        if let Some(last) = get_last {
            n = count - 1 - last;
        } else {
            n = 0;
        }
        last_spoken.insert(prev_num, count - 1);
        prev_num = n;
        count += 1;
    }
    println!("{}'th number spoken: {}", count, prev_num);
}
