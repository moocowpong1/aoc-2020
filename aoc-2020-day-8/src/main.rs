use std::io;

#[derive(Debug)]
enum Instruction {
    NOP(i32),
    ACC(i32),
    JMP(i32),
}

#[derive(Debug)]
enum ProgramOutcome {
    Ok(i32),        // normal termination executing the instruction after the end of the program    Ok(acc)
    OutOfBounds(isize, i32),  // tried to execute any other instruction outside the program       Out_Of_Bounds(pc, acc)
    Loop(isize, i32),   // infinite looped                                                          Loop(pc, acc)

}

fn main() {
    let mut program: Vec<Instruction> = Vec::new();

    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read from stdin");
        let trimmed = input.trim();
        if trimmed.len() == 0 { break; }

        let val = trimmed[4..].parse().expect("Couldn't read number");

        let instruction = match &trimmed[0..3] {
            "nop" => Instruction::NOP(val),
            "acc" => Instruction::ACC(val),
            "jmp" => Instruction::JMP(val),
            _ => panic!("Invalid instruction"),
        };

        program.push(instruction);
    }

    println!("Initial program result: {:?}", run_program(&program));

    //Now iterate through swapping jmp's for nop's.
    for i in 0..program.len() {
        if !swap_instruction(&mut program[i]) { continue; } //attempt to swap the instruction; if nothing changed, go to the next loop

        let outcome = run_program(&program);

        println!("Swapping instruction {} to {:?} yields {:?}", i, program[i], outcome);

        match outcome {
            ProgramOutcome::Ok(n) => { println!("Success! Final accumulator: {}", n); break; }
            _ => {}
        }

        swap_instruction(&mut program[i]); //swap back before the next loop
    }

}

fn swap_instruction(inst: &mut Instruction) -> bool { // Return value indicates if anything was swapped
    match inst {
        Instruction::NOP(n) => { *inst = Instruction::JMP(*n); return true; }
        Instruction::JMP(n) => { *inst = Instruction::NOP(*n); return true; }
        Instruction::ACC(_) => { return false; }
    }
}

fn run_program(program: &Vec<Instruction>) -> ProgramOutcome {
    let length = program.len();
    let mut visited: Vec<bool> = vec![false; length];

    let mut acc = 0i32;
    let mut pc = 0isize;

    loop {
        if pc < 0 { return ProgramOutcome::OutOfBounds(pc, acc); }
        let pcu = pc as usize;
        if pcu > length { return ProgramOutcome::OutOfBounds(pc, acc); }
        if pcu == length { return ProgramOutcome::Ok(acc); }
        if visited[pcu] { return ProgramOutcome::Loop(pc, acc); }
        
        visited[pcu] = true;

        match program[pcu] {
            Instruction::NOP(_) => { pc += 1; }
            Instruction::ACC(n) => { acc += n;  pc += 1; }
            Instruction::JMP(offset) => { pc = pc + (offset as isize); }
        }
    }
}