
#![feature(test)]

extern crate test;

use std::env;
use std::fs;
use std::error::Error;
use std::collections::BTreeSet;
use std::collections::BTreeMap;

fn main() -> Result<(), Box<dyn Error>> {
    let input = fs::read_to_string(env::args().nth(1).unwrap_or(String::from("puzzle_input.txt")))?;

    println!("Part 1:");
    part1(&input);

    println!("\nPart 2:");
    part2(&input);

    Ok(())
}

struct Food<'a> {
    ingredients: BTreeSet<&'a str>,
    known_allergens: BTreeSet<&'a str>,
}

fn part1(input: &str) {
    let mut foods = Vec::new();
    for line in input.split('\n') {
        let mut ingredients: BTreeSet<&str> = BTreeSet::new();
        let mut known_allergens: BTreeSet<&str> = BTreeSet::new();
        let parts: Vec<&str> = line.split('(').collect();
        
        for ingredient in parts[0].trim().split(' ') {
            ingredients.insert(ingredient);
        }

        let allergen_part = parts[1];   // has the form "contains xxx, yyy)"
        for allergen in allergen_part[9..(allergen_part.len() - 1)].split(", ") {
            known_allergens.insert(allergen);
        }
        foods.push(Food{ known_allergens, ingredients });
    }

    let all_ingredients: BTreeSet<&str> = foods.iter().flat_map(|f| f.ingredients.iter()).copied().collect();

    let mut allergen_possible_ingredients: BTreeMap<&str, BTreeSet<&str>> = BTreeMap::new();

    for food in &foods {
        for allergen in &food.known_allergens {
            if let Some(poss_ingreds) = allergen_possible_ingredients.get_mut(allergen) {
                *poss_ingreds = poss_ingreds.intersection(&food.ingredients).copied().collect();
            } else {
                allergen_possible_ingredients.insert(allergen, food.ingredients.clone());
            }
        }
    }

    let potential_allergen_ingredients: BTreeSet<&str> = allergen_possible_ingredients.values().flatten().copied().collect();
    let mut total_num_occurrances = 0;
    for known_safe_ingredient in all_ingredients.difference(&potential_allergen_ingredients) {
        let num_occurrances = foods.iter().filter(|f| f.ingredients.contains(known_safe_ingredient)).count();
        total_num_occurrances += num_occurrances;
        #[cfg(debug_assertions)]
        println!("{} is known to be safe; occurs {} times", known_safe_ingredient, num_occurrances);
    }

    println!("Total occurrances of safe ingredients: {}", total_num_occurrances);
}

fn part2(input: &str) {
    let mut foods = Vec::new();
    for line in input.split('\n') {
        let mut ingredients: BTreeSet<&str> = BTreeSet::new();
        let mut known_allergens: BTreeSet<&str> = BTreeSet::new();
        let parts: Vec<&str> = line.split('(').collect();
        
        for ingredient in parts[0].trim().split(' ') {
            ingredients.insert(ingredient);
        }

        let allergen_part = parts[1];   // has the form "contains xxx, yyy)"
        for allergen in allergen_part[9..(allergen_part.len() - 1)].split(", ") {
            known_allergens.insert(allergen);
        }
        foods.push(Food{ known_allergens, ingredients });
    }

    let all_allergens: BTreeSet<&str> = foods.iter().flat_map(|f| f.known_allergens.iter()).copied().collect();

    let mut allergen_possible_ingredients: BTreeMap<&str, BTreeSet<&str>> = BTreeMap::new();

    for food in &foods {
        for allergen in &(food.known_allergens) {
            if let Some(poss_ingreds) = allergen_possible_ingredients.get_mut(allergen) {
                *poss_ingreds = poss_ingreds.intersection(&food.ingredients).copied().collect();
            } else {
                allergen_possible_ingredients.insert(allergen, food.ingredients.clone());
            }
        }
    }

    let num_allergens = all_allergens.len();

    let mut allergen_map: BTreeMap<&str, &str> = BTreeMap::new();

    while allergen_map.len() < num_allergens {
        let mut found_allergen = None;
        let mut found_ingred = None;
        for (allergen, poss_ingreds) in allergen_possible_ingredients.iter() {
            if poss_ingreds.len() != 1 {
                continue;
            }
            found_allergen = Some(*allergen);
            found_ingred = Some(*poss_ingreds.iter().next().unwrap());
            break;
        }
        let found_allergen = found_allergen.expect("Couldn't find a constrained allergen");
        let found_ingred = found_ingred.unwrap();

        allergen_possible_ingredients.remove(found_allergen);
        for poss_ingreds in allergen_possible_ingredients.values_mut() {
            poss_ingreds.remove(found_ingred);
        }
        #[cfg(debug_assertions)]
        println!("Found allergen! {} => {}", found_allergen, found_ingred);

        allergen_map.insert(found_allergen, found_ingred);
    }

    println!("Canonical dangerous ingredient list:");
    let mut dangerous_ingred_iter = allergen_map.values();
    print!("{}", dangerous_ingred_iter.next().unwrap());
    for ingred in dangerous_ingred_iter {  // sorted by key (allergen name) because it's a BTreeSet
        print!(",{}", ingred);
    }
    println!();
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn bench_part_1(b: &mut Bencher) {
        let input = fs::read_to_string("puzzle_input.txt").unwrap();
        b.iter(|| part1(&input));
    }

    #[bench]
    fn bench_part_2(b: &mut Bencher) {
        let input = fs::read_to_string("puzzle_input.txt").unwrap();
        b.iter(|| part2(&input));
    }
}