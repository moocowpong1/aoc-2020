use std::io;

#[derive(Clone, Debug, PartialEq, Eq)]
enum Seat {
    None,
    Empty,
    Full,
}

fn main() {
    let mut seat_arrangement: Vec<Vec<Seat>> = Vec::new();

    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read from stdin");
        let trimmed = input.trim();
        if trimmed.len() == 0 { break; }

        let row:Vec<Seat> = trimmed.chars().map(as_seat).collect();

        seat_arrangement.push(row);
    }

    loop {
        let (new_arrangement, changed) = evolve(seat_arrangement);
        seat_arrangement = new_arrangement;
        if !changed { break; }
    }

    let num_full_seats:usize = seat_arrangement.into_iter().map(|row| { row.into_iter().filter(|seat| { *seat == Seat::Full }).count() }).sum();

    println!("Number of filled seats:{}",num_full_seats);
}

fn pad(arr:&mut Vec<Vec<Seat>>) {
    let width = arr[0].len();

    for row in arr.iter_mut() {
        row.insert(0,Seat::Empty);
        row.push(Seat::Empty);
    }

    arr.insert(0, vec![Seat::Empty; width + 2]);
    arr.push(vec![Seat::Empty; width + 2]);
}

fn as_seat(ch:char) -> Seat {
    match ch {
        '.' => Seat::None,
        'L' => Seat::Empty,
        '#' => Seat::Full,
        _ => panic!("Failed to read seat"),
    }
}

//nbhd is always 3x3 but fixed sized arrays don't seem to work well for expressing that
fn ca_rule(nbhd: &[&[Seat]]) -> Seat {
    let mut occupied = 0u32;        // will be the number of occupied neighbors, counting itself.
    for r in nbhd.iter() {
        for s in r.iter() {
            if *s == Seat::Full { occupied += 1 }
        }
    }
    match nbhd[1][1] {
        Seat::None => Seat::None,
        Seat::Empty => if occupied == 0 { Seat::Full } else { Seat::Empty },    //empty seats with no neighbors are filled
        Seat::Full => if occupied >= 5 { Seat::Empty } else { Seat::Full },
    }
}

//Return value is the new array, and whether it's changed.
fn evolve(mut arr: Vec<Vec<Seat>>) -> (Vec<Vec<Seat>>, bool) {
    let height = arr.len();
    let width = arr[0].len();
    let mut changed = false;

    pad(&mut arr); // to handle boundary conditions

    let mut result = Vec::new();
    result.reserve(height); //to avoid multiple resizes as we fill it

    for r in 0..height {   //iterate over rows
        let mut result_row = Vec::new();
        result_row.reserve(width);
        for c in 0..width {
            let old_cell = &arr[r+1][c+1];
            let nbhd:Vec<&[Seat]> = arr[r..(r+3)].iter().map(|row| { &row[c..(c+3)] }).collect();
            let new_cell = ca_rule(nbhd.as_slice());
            changed = changed || (&new_cell != old_cell);
            result_row.push(new_cell);
        }
        result.push(result_row);
    }

    (result, changed)
}