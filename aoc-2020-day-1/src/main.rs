use std::io;
use std::cmp::Ordering;

fn main() {
    let mut vec = Vec::<u32>::new();
    loop
    {
        let mut input_text = String::new();
        io::stdin().read_line(&mut input_text).expect("Failed to read");
        let trimmed = input_text.trim();
        match trimmed.parse::<u32>() {
            Ok(i) => vec.push(i),
            Err(..) => break,
        }
    }

    vec.sort_unstable();

    let n = vec.len();

    let mut ix_low = 0;
    let mut ix_high = n-1;
    
    loop
    {
        if ix_low > ix_high
        {
            println!("No two entries summing to 2020 found.");
            break;
        }
        let n_low = vec[ix_low];
        let n_high = vec[ix_high];

        let sum = n_low + n_high;

        match sum.cmp(&2020) {
            Ordering::Less => ix_low += 1,
            Ordering::Greater => ix_high -= 1,
            Ordering::Equal => { println!("n_low: {}, n_high: {}, sum: {}, product: {}", n_low, n_high, n_low + n_high, n_low * n_high); break; },
        }
    }
    
    ix_low = 0;
    let mut ix_mid;

    'outer: loop
    {
        let n_low = vec[ix_low];
        let target = 2020 - n_low;  // repeat the original problem inside the loop with a lower target
        if 3*n_low >= 2020
        {
            println!("No three entries summing to 2020 found.");
            break;
        }
        
        ix_mid = ix_low + 1;
        ix_high = n-1;
        loop
        {
            if ix_mid > ix_high {
                ix_low += 1;
                break;
            }
            
            let n_mid = vec[ix_mid];
            let n_high = vec[ix_high];

            let sum = n_mid + n_high;

            match sum.cmp(&target) {
                Ordering::Less => ix_mid += 1,
                Ordering::Greater => ix_high -= 1,
                Ordering::Equal => { println!("n_low: {}, n_mid: {}, n_high: {}, sum: {}, product: {}", 
                                                n_low, n_mid, n_high, n_low + n_mid + n_high, n_low * n_mid * n_high); break 'outer; },
            }
        }
    }
}