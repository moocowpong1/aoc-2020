/*
 * Alternate solution to day 8 which nondeterministically executes the program to account for all possible
 * instruction changes in one pass.
 */

use std::io;

#[derive(Debug)]
enum Instruction {
    NOP(i32),
    ACC(i32),
    JMP(i32),
}

#[derive(Clone)]
struct ProgramState {
    pc: isize,
    acc: i32,
    is_primary: bool,
    instruction_changed: Option<usize>,
}

fn main() {
    let mut program: Vec<Instruction> = Vec::new();

    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read from stdin");
        let trimmed = input.trim();
        if trimmed.len() == 0 { break; }

        let val = trimmed[4..].parse().expect("Couldn't read number");

        let instruction = match &trimmed[0..3] {
            "nop" => Instruction::NOP(val),
            "acc" => Instruction::ACC(val),
            "jmp" => Instruction::JMP(val),
            _ => panic!("Invalid instruction"),
        };

        program.push(instruction);
    }

    nondet_run_program(&program);

}

fn alternate_instruction(inst: &Instruction) -> Option<Instruction> { 
    match inst {
        Instruction::NOP(n) => { return Some(Instruction::JMP(*n)); }
        Instruction::JMP(n) => { return Some(Instruction::NOP(*n)); }
        Instruction::ACC(_) => { return None; }
    }
}

//run the program branching along all one-instruction possibilities until one of them terminates properly
//"primary" means that this branch of execution is following the original program, meaning it still has
// the potential to branch.
fn nondet_run_program(program: &Vec<Instruction>) {
    let length = program.len();
    let mut primary_visited: Vec<bool> = vec![false; length];
    let mut alternate_visited: Vec<bool> = vec![false; length];
    let mut time = 0u32;

    let mut states = vec![ProgramState { pc: 0, acc: 0, is_primary: true, instruction_changed:None }];
    let mut final_state = None;
    primary_visited[0] = true;

    loop {
        time += 1;
        let stepped_states_iter = states.iter()
                                     .flat_map(|s| { step_states(s.pc, &program[s.pc as usize], s) }); // step each state to its next state(s)
        let mut next_states = Vec::new();
        println!("t = {}", time);
        print!("States: ");
        for ns in stepped_states_iter {
            if (ns.pc < 0) || (ns.pc as usize > length) { continue; } //filter out states that are out of bounds
            if ns.pc as usize == length {                             // if this state represents successful termination, record it
                final_state = Some(ns.clone());
            } else {
                if primary_visited[ns.pc as usize] { continue; }          //if not, filter out if the primary execution path has already visited here
                if (!ns.is_primary) && alternate_visited[ns.pc as usize] { continue; }  // or alternate states if an alternate state has already visited here

                if ns.is_primary {
                    primary_visited[ns.pc as usize] = true;
                }
                else {
                    alternate_visited[ns.pc as usize] = true;
                }
            }
            
            print!("({},acc={}){}, ", ns.pc, ns.acc, if ns.is_primary { "*" } else { "" });
            next_states.push(ns);
        }
        print!("\n");
        if let Some(fs) = final_state {
            println!("Success! Instruction changed: {}, Final accumulator value: {}", 
                     if let Some(is) = fs.instruction_changed { is.to_string() } else { String::from("None") }, 
                     fs.acc);
            println!("Instructions visited by primary execution path: {} Alternate: {}", 
                     primary_visited.iter().filter(|b| { return **b }).count(), 
                     alternate_visited.iter().filter(|b| {return **b }).count());
            break;
        }
        states = next_states;
    }
}

//One or two possible states following the given state with the given instruction.
//Only returns a second state if a branch is possible, i.e. if state.is_primary. The second state
//will have is_primary: false. 
fn step_states(pc: isize, inst:&Instruction, state:&ProgramState) -> Vec<ProgramState> {
    let mut nsvec = Vec::new();
    nsvec.push(step_state(inst, state));
    if state.is_primary {
        if let Some(alt_inst) = alternate_instruction(inst) {
            nsvec.push(step_state(&alt_inst, &ProgramState { is_primary: false, instruction_changed: Some(pc as usize), ..*state }))
        }
    }
    return nsvec;
}

fn step_state(inst:&Instruction, state:&ProgramState) -> ProgramState {
    return match inst {
        Instruction::NOP(_) => ProgramState { pc: state.pc + 1, ..*state },
        Instruction::ACC(n) => ProgramState { pc: state.pc + 1, acc: state.acc + *n, ..*state},
        Instruction::JMP(offset) => ProgramState { pc: state.pc + (*offset as isize), ..*state},
    }
}


