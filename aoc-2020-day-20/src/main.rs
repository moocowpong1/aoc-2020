use std::env;
use std::fs;
use std::mem;
use std::error::Error;
use std::time::Instant;
use std::collections::BTreeMap;
use bit_reverse::BitwiseReverse;
use transpose::transpose_inplace;

fn main() -> Result<(), Box<dyn Error>> {
    let input = fs::read_to_string(env::args().nth(1).unwrap_or(String::from("puzzle_input.txt")))?;
    let input2 = input.clone();
    let part1_start = Instant::now();
    part1(input);
    let part1_duration = part1_start.elapsed();
    println!("Part 1 time: {:?}\n", part1_duration);

    let part2_start = Instant::now();
    part2(input2);
    let part2_duration = part2_start.elapsed();
    println!("Part 2 time: {:?}\n", part2_duration);

    Ok(())
}

fn reverse_10_bits(x: u16) -> u16 {
    x.swap_bits() >> 6
}


// This part is a mess; I was mostly just trying to understand the data, then found the corner pieces based
// on the fact that two of their edge patterns don't recur.
fn part1(input: String) {
    let mut counts: BTreeMap<u16, usize> = BTreeMap::new();
    for tile in input.split("\n\n") {
        let lines:Vec<&str> = tile.lines().collect();
        let mut top = 0u16;
        let mut bottom = 0u16;
        let mut right = 0u16;
        let mut left = 0u16;
        for b in lines[1].as_bytes() {
            top <<= 1;
            if *b == b'#' { top += 1 }
        }
        for b in lines[10].as_bytes() {
            bottom <<= 1;
            if *b == b'#' { bottom += 1 }
        }
        for i in 1..=10 {
            left <<= 1;
            right <<= 1;
            if lines[i].as_bytes()[0] == b'#' { left += 1 }
            if lines[i].as_bytes()[9] == b'#' { right += 1 }
        }
        if let Some(top_count) = counts.get_mut(&top) { *top_count += 1; } else { counts.insert(top, 1); }
        if let Some(bottom_count) = counts.get_mut(&bottom) { *bottom_count += 1; } else { counts.insert(bottom, 1); }
        if let Some(left_count) = counts.get_mut(&left) { *left_count += 1; } else { counts.insert(left, 1); }
        if let Some(right_count) = counts.get_mut(&right) { *right_count += 1; } else { counts.insert(right, 1); }
        let top = reverse_10_bits(top);
        let bottom = reverse_10_bits(bottom);
        let left = reverse_10_bits(left);
        let right = reverse_10_bits(right);
        if let Some(top_count) = counts.get_mut(&top) { *top_count += 1; } else { counts.insert(top, 1); }
        if let Some(bottom_count) = counts.get_mut(&bottom) { *bottom_count += 1; } else { counts.insert(bottom, 1); }
        if let Some(left_count) = counts.get_mut(&left) { *left_count += 1; } else { counts.insert(left, 1); }
        if let Some(right_count) = counts.get_mut(&right) { *right_count += 1; } else { counts.insert(right, 1); }
    }
    let mut corner_product = 1;
    for tile in input.split("\n\n") {
        let lines:Vec<&str> = tile.lines().collect();
        let id_line = lines[0];
        let tile_id = (&id_line[5..(id_line.len() - 1)]).parse::<usize>().unwrap();
        let mut top = 0u16;
        let mut bottom = 0u16;
        let mut right = 0u16;
        let mut left = 0u16;
        for b in lines[1].as_bytes() {
            top <<= 1;
            if *b == b'#' { top += 1 }
        }
        for b in lines[10].as_bytes() {
            bottom <<= 1;
            if *b == b'#' { bottom += 1 }
        }
        for i in 1..=10 {
            left <<= 1;
            right <<= 1;
            if lines[i].as_bytes()[0] == b'#' { left += 1 }
            if lines[i].as_bytes()[9] == b'#' { right += 1 }
        }
        let mut num_non_matching_edges = 0usize;
        if *counts.get(&top).unwrap() == 1 { num_non_matching_edges += 1; }
        if *counts.get(&bottom).unwrap() == 1 { num_non_matching_edges += 1; }
        if *counts.get(&left).unwrap() == 1 { num_non_matching_edges += 1; }
        if *counts.get(&right).unwrap() == 1 { num_non_matching_edges += 1; }
        
        if num_non_matching_edges >= 2 {
            println!("Tile {} has {} non-matching edges", tile_id, num_non_matching_edges);
            corner_product *= tile_id;
            println!("Counts for {}: top {}, bottom {}, left {}, right {}", tile_id, 
                    *counts.get(&top).unwrap(), *counts.get(&bottom).unwrap(), *counts.get(&left).unwrap(), *counts.get(&right).unwrap())
        }
    }

    println!("Corner product: {}", corner_product);

    let max_count = counts.values().copied().max().unwrap();
    let mut histogram = vec![0usize; max_count + 1];

    for count in counts.values().copied() {
        histogram[count] += 1;
    }

    for (count, num) in histogram.iter().enumerate() {
        println!("{}: {}", count, num);
    }

    
}

/*
 * Data from Part 1 demonstrates that every edge pattern shows up exactly either once or twice - once if it's on the edge of the 12x12 image, twice
 * otherwise. We can exploit that here. We also use the fact that each section of the input is 10x10, meaning the image portion of the tile is
 * 8x8, and that based on the data from part 1, the tiles seem to form a 12x12 grid.
 */
fn part2(input: String) {
    let mut tiles_by_left: BTreeMap<u16, Vec<Tile>> = BTreeMap::new();
    let mut tiles_by_top: BTreeMap<u16, Vec<Tile>> = BTreeMap::new();

    let mut tile_assembly: Vec<Tile> = Vec::new();

    for tile_desc in input.split("\n\n") {
        let mut lines = tile_desc.lines();
        let id_string = lines.next().unwrap();
        let tile_id = id_string[5..(id_string.len() - 1)].parse::<usize>().unwrap();

        let mut lines = lines.map(|l| l.as_bytes());

        let mut top = 0u16;
        let mut bottom = 0u16;
        let mut left = 0u16;
        let mut right = 0u16;
        let mut tile_image: Vec<bool> = Vec::new();

        let top_line = lines.next().unwrap();
        if top_line[0] == b'#' { left += 1; }
        if top_line[9] == b'#' { right += 1; }
        for b in top_line {
            top <<= 1;
            if *b == b'#' { top += 1; }
        }

        for line in (&mut lines).take(8) {
            left <<= 1;
            right <<= 1;
            if line[0] == b'#' { left += 1; }
            if line[9] == b'#' { right += 1; }
            let mut row: Vec<bool> = line[1..9].iter().map(|b| *b == b'#').collect();
            tile_image.append(&mut row);
        }

        let bottom_line = lines.next().unwrap();
        left <<= 1;
        right <<= 1;
        if bottom_line[0] == b'#' { left += 1; }
        if bottom_line[9] == b'#' { right += 1; }
        for b in bottom_line {
            bottom <<= 1;
            if *b == b'#' { bottom += 1; }
        }

        let tile = Tile { id: tile_id, top, bottom, left, right, image: tile_image };

        if tile_id == 1439 {
            // We know from Part 1 that tile 1439 is a corner tile which starts out in a good orientation;
            // we take it as the upper left tile and the starting point of our process.
            tile_assembly.push(tile.clone());
        }
        
        // We need to be able to use the tile in any orientation, and access it by its left or top edges.
        let tile_orients = tile.all_orientations();
        for tile_orient in tile_orients {
            if let Some(top_vec) = tiles_by_top.get_mut(&tile_orient.top) {
                top_vec.push(tile_orient.clone());
            } else {
                tiles_by_top.insert(tile_orient.top, vec![tile_orient.clone()]);
            }
            if let Some(left_vec) = tiles_by_left.get_mut(&tile_orient.left) {
                left_vec.push(tile_orient);
            } else {
                tiles_by_left.insert(tile_orient.left, vec![tile_orient]);
            }
        }
    }

    // Assemble the whole image. We could keep track of a list of possibilities at each step, but we know there's at most one other tile
    // that fits any edge of any tile, so we can simply take the first suitable choice in each case and be certain that they'll fit together in the end.
    // Note that we need to be careful not to match a tile to itself, because flipping a tile horizontally or vertically will produce a "match".
    for i in 1..12*12 {
        if i%12 == 0 {  // we've started a new row of the image, we need to move down
            let prev_tile = &tile_assembly[i-12];
            let tile_matches = tiles_by_top.get(&prev_tile.bottom).unwrap().iter();
            let new_tile = tile_matches.filter(|t| t.id != prev_tile.id).next().unwrap();
            tile_assembly.push(new_tile.clone());
        } else {
            let prev_tile = &tile_assembly[i-1];
            let tile_matches = tiles_by_left.get(&prev_tile.right).unwrap().iter();
            let new_tile = tile_matches.filter(|t| t.id != prev_tile.id).next().unwrap();
            tile_assembly.push(new_tile.clone());
        }
    }

    let mut image = assemble_image(&tile_assembly);
    println!("Assembled image:");
    for (i, b) in image.iter().enumerate() {
        print!("{}", if *b { '#' } else { '.' });
        if i % (12 * 8) == 12 * 8 - 1 {
            println!();
        }
    }
    println!();
    let (sea_monster_map, num_sea_monsters) = find_monsters(&mut image);
    let mut num_waves = 0usize;
    println!("{} sea monsters found!", num_sea_monsters);
    for (i, (b, s)) in image.iter().zip(sea_monster_map.iter()).enumerate() {
        if *s {
            print!("O");
        } else {
            if *b {
                num_waves += 1;
                print!("#");
            } else {
                print!(".");
            }
        }
        if i % (12 * 8) == 12 * 8 - 1 {
            println!();
        }
    }
    println!("Number of non-sea-monster #'s: {}", num_waves);
}

// Each edge's pattern is stored as a 10-bit integer, reading left to right and top to bottom.
#[derive(Clone, PartialEq, Eq, Debug)]
struct Tile {
    id: usize,
    top: u16,
    bottom: u16,
    left: u16,
    right: u16,
    image: Vec<bool>,   // 8x8, row major
}

impl Tile {
    // Flips the tile as though in a vertical mirror through its center.
    fn flip_h(&mut self) {
        // Reverse top and bottom, exchange left and right.
        self.top = reverse_10_bits(self.top);
        self.bottom = reverse_10_bits(self.bottom);
        mem::swap(&mut self.right, &mut self.left);
        // Flip the image portion of the tile left-to-right.
        flip_h_image(&mut self.image, 8, 8);
    }

    // Flips the tile as though in a diagonal miror through its upper left and lower right corners.
    fn flip_d(&mut self) {
        //Exchange top with left and bottom with right.
        mem::swap(&mut self.top, &mut self.left);
        mem::swap(&mut self.bottom, &mut self.right);
        // Flip the image portion of the tile. (Transpose row-major to column-major using the transpose crate.)
        flip_d_image(&mut self.image, 8, 8);
    }

    // The two above operations generate the 8-element dihedral group; this applies all of them to the given Tile.
    fn all_orientations(self) -> Vec<Tile> {
        let mut tile = self;
        let mut result = Vec::new();
        for _ in 0..3 {
            result.push(tile.clone());
            tile.flip_h();
            result.push(tile.clone());
            tile.flip_d();
        }
        result.push(tile.clone());
        tile.flip_h();
        result.push(tile);
        result
    }
}

// We use these for individual tiles and the whole composed image.
fn flip_h_image(image: &mut [bool], width: usize, height: usize) {
    for row in 0..height {
        let row_slice = &mut image[width*row..width*(row+1)];
        row_slice.reverse();
    }
}

fn flip_d_image(image: &mut [bool], width: usize, height: usize) {
    let mut scratch = vec![false; usize::max(width, height)];
    transpose_inplace(image, &mut scratch, width, height);
}

fn assemble_image(assembly: &Vec<Tile>) -> Vec<bool> {
    let mut image: Vec<bool> = Vec::new();
    for assembly_row_num in 0..12 {
        let assembly_row = &assembly[12*assembly_row_num..12*(assembly_row_num+1)];
        for row_num in 0..8 {
            for tile in assembly_row.iter() {
                let mut image_row = Vec::from(&tile.image[8*row_num..8*(row_num + 1)]);
                image.append(&mut image_row);
            }
        }
    }
    image
}

// Rotates and flips the image and checks each orientation for sea monsters.
fn find_monsters(image: &mut Vec<bool>) -> (Vec<bool>, usize) {
    let mut monster_map = vec![false; (12*8)*(12*8)];
    let mut num_monsters = 0usize;
    for _ in 0..4 {
        flip_h_image(image, 12*8, 12*8);
        flip_h_image(&mut monster_map, 12*8, 12*8);
        num_monsters += find_monsters_in_orient(image, &mut monster_map);
        flip_d_image(image, 12*8, 12*8);
        flip_d_image(&mut monster_map, 12*8, 12*8);
        num_monsters += find_monsters_in_orient(image, &mut monster_map);
    }
    (monster_map, num_monsters)
}

// Sea monster:
//                   # 
// #    ##    ##    ###
//  #  #  #  #  #  #   
fn find_monsters_in_orient(image: & Vec<bool>, monster_map: &mut Vec<bool>) -> usize {
    const SEA_MONSTER: [[bool;20];3] = [[false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true,  false],
                                        [true,  false, false, false, false, true,  true,  false, false, false, false, true,  true,  false, false, false, false, true,  true,  true ],
                                        [false, true,  false, false, true,  false, false, true,  false, false, true,  false, false, true,  false, false, true,  false, false, false]];
    
    let mut num_monsters = 0usize;

    for row in 0..(12*8 - 3) {
        'offset: for offset in 0..(12 * 8 - 20) {
            let monster_position = 12*8*row + offset; //absolute index of the upper left corner of the potential monster
            for monster_row in 0..3 {
                let row_pos = monster_position + 12*8*monster_row;
                if !compare_rows(&image[row_pos..(row_pos + 20)], &SEA_MONSTER[monster_row]) {
                    continue 'offset;
                }
            }
            // found a match!
            num_monsters += 1;
            for monster_row in 0..3 {
                for (monster_bit, map_bit) in SEA_MONSTER[monster_row].iter().zip(
                                                monster_map[(monster_position + 12*8*monster_row)..(monster_position + 12*8*(monster_row+1))].iter_mut()) {
                    if *monster_bit { *map_bit = true; }
                }
            }
        }
    }

    num_monsters
}

fn compare_rows(image_row: &[bool], monster_row: &[bool]) -> bool
{
    for (image_bit, monster_bit) in image_row.iter().zip(monster_row.iter()) {
        if *monster_bit && !(*image_bit) {
            return false;
        }
    }
    true
}