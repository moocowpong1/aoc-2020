
#![feature(test)]

extern crate test;

use std::env;
use std::fs;
use std::error::Error;
mod part1;
mod part2;

fn main() -> Result<(), Box<dyn Error>> {
    let input = fs::read_to_string(env::args().nth(1).unwrap_or(String::from("puzzle_input.txt")))?;

    println!("Part 1:");
    part1::part1(&input);

    println!("\nPart 2:");
    part2::part2(&input);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn bench_part_1(b: &mut Bencher) {
        let input = fs::read_to_string("puzzle_input.txt").unwrap();
        b.iter(|| part1::part1(&input));
    }

    #[bench]
    fn bench_part_2(b: &mut Bencher) {
        let input = fs::read_to_string("puzzle_input.txt").unwrap();
        b.iter(|| part2::part2(&input));
    }
}