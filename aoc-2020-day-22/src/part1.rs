

use std::collections::VecDeque;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Winner {
    Player1,
    Player2,
}

pub fn parse_input(input: &str) -> (VecDeque<usize>, VecDeque<usize>) {
    let mut sections = input.split("\n\n");
    let mut deck1 = VecDeque::new();
    let mut deck2 = VecDeque::new();
    for line in sections.next().unwrap().lines().skip(1) {
        deck1.push_back(line.parse().unwrap());
    }
    for line in sections.next().unwrap().lines().skip(1) {
        deck2.push_back(line.parse().unwrap());
    }
    return (deck1, deck2);
}

fn combat_round(deck1: &mut VecDeque<usize>, deck2: &mut VecDeque<usize>) -> Option<Winner> {
    if deck1.len() == 0 { return Some(Winner::Player2); }
    if deck2.len() == 0 { return Some(Winner::Player1); }
    let top1 = deck1.pop_front().unwrap();
    let top2 = deck2.pop_front().unwrap();
    if top1 > top2 { deck1.push_back(top1); deck1.push_back(top2); }
    if top2 > top1 { deck2.push_back(top2); deck2.push_back(top1); }
    None
}

pub fn calculate_score(deck: &VecDeque<usize>) -> usize {
    let mut total: usize = 0;
    for (i, card) in deck.iter().rev().enumerate() {
        total += (i + 1) * *card;
    }
    return total;
}

pub fn part1(input: &str) {
    let (mut deck1, mut deck2) = parse_input(input);
    let winner: Winner;
    loop {
        if let Some(win) = combat_round(&mut deck1, &mut deck2) {
            winner = win;
            break;
        }
    }
    match winner {
        Winner::Player1 => println!("Player 1 wins with score: {}", calculate_score(&deck1)),
        Winner::Player2 => println!("Player 2 wins with score: {}", calculate_score(&deck2)),
    }
}

