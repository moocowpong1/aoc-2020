
use crate::part1::{self, Winner::{self, Player1, Player2}};
use std::collections::VecDeque;
use std::collections::BTreeSet;
#[cfg(debug_assertions)]
use std::io;

pub fn part2(input: &str) {
    #[cfg(debug_assertions)]
    println!("Running with debug code steps through the game state one round at a time; use --release to run without delays.");
    let (deck1, deck2) = part1::parse_input(input);
    let (winner, winning_deck) = play_recursive_combat(deck1, deck2);
    match winner {
        Player1 => println!("Player 1 wins with score: {}", part1::calculate_score(&winning_deck)),
        Player2 => println!("Player 2 wins with score: {}", part1::calculate_score(&winning_deck)),
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
struct GameState {
    deck1: VecDeque<usize>, 
    deck2: VecDeque<usize>,
}

// We play the game iteratively, using a stack to track the state of supergames. Rather than store the state we left them in,
// midway through a turn, we store the state of the decks going into the turn and when we go back up to the supergame, we
// rerun the turn with knowledge of who the winner of the subgame was/will be.
// Returns the overall winner and the state of their deck at the end of the game.
fn play_recursive_combat(deck1: VecDeque<usize>, deck2: VecDeque<usize>) -> (Winner, VecDeque<usize>) {
    #[cfg(debug_assertions)]
    println!("{:?}, {:?}", &deck1, &deck2);
    let mut superko_set: BTreeSet<GameState> = BTreeSet::new();
    let mut game_stack: Vec<(GameState, BTreeSet<GameState>)> = Vec::new();
    let mut current_game: GameState = GameState { deck1, deck2 };
    let mut subgame_winner: Option<Winner> = None;
    #[cfg(debug_assertions)]
    let mut delay_input = String::new();
    loop {
        #[cfg(debug_assertions)]
        io::stdin().read_line(&mut delay_input).unwrap();
        let round_winner: Winner;
        let game_winner: Winner;
        // First, if the round is *not* an intentional rerun we want to record the game state and check if it's a repetition
        if subgame_winner.is_none() && superko_set.replace(current_game.clone()).is_some() {
            #[cfg(debug_assertions)]
            println!("Repetition: {:?}, {:?}", &current_game.deck1, &current_game.deck2);
            game_winner = Player1;  // Player 1 wins the current subgame by repetition.
        } else {
            #[cfg(debug_assertions)]
            println!("{:?}, {:?}", &current_game.deck1, &current_game.deck2);
            let top1 = current_game.deck1.pop_front().unwrap();     // We handle empty decks at the end of a round, meaning it's impossible to 
            let top2 = current_game.deck2.pop_front().unwrap();     // start a round with an empty deck unless you called the function with one.
            #[cfg(debug_assertions)]
            println!("{}, {}", top1, top2);
            #[cfg(debug_assertions)]
            println!("game_stack.len(): {}, deck1.len(): {}, deck2.len(): {}", game_stack.len(), current_game.deck1.len(), current_game.deck2.len());
            // We have enough cards for the subgame.
            if (top1 <= current_game.deck1.len()) && (top2 <= current_game.deck2.len()) {
                // First, check if we already played the subgame - this might be a rerun of the round!
                if let Some(winner) = subgame_winner.take() {
                    round_winner = winner;  // in this case, the winner of the subgame wins the round as well
                } else {
                    let subgame_deck1 = current_game.deck1.iter().take(top1).copied().collect();
                    let subgame_deck2 = current_game.deck2.iter().take(top2).copied().collect();
                    current_game.deck1.push_front(top1);    // put the cards back so we can rerun the round.
                    current_game.deck2.push_front(top2);
                    game_stack.push((current_game, superko_set));
                    superko_set = BTreeSet::new(); // new subgames get a clean superko_set, but save the old one for when we return
                    current_game = GameState { deck1: subgame_deck1, deck2: subgame_deck2 };
                    continue;   // skip ahead to the subgame
                }
            } else {    // We don't have enough cards; decide based on higher card.
                round_winner = if top1 > top2 { Player1 } else { Player2 };
            }
            match round_winner {
                Player1 => { 
                    current_game.deck1.push_back(top1);
                    current_game.deck1.push_back(top2);
                    if current_game.deck2.len() == 0 {      // check for Player2 loss
                        game_winner = Player1;
                    } else {
                        continue;   // continue to next round.
                    }
                },
                Player2 => { 
                    current_game.deck2.push_back(top2);
                    current_game.deck2.push_back(top1);
                    if current_game.deck1.len() == 0 {
                        game_winner = Player2;
                    } else {
                        continue;
                    }
                },
            }
        }
        // We have already continued if we went to a new round of the same game; so if we're here we have a game winner.
        if let Some((supergame, prev_superko_set)) = game_stack.pop() {
            subgame_winner = Some(game_winner);
            current_game = supergame;
            superko_set = prev_superko_set;
            continue;   // return to the supergame and rerun the round with known subgame_winner
        } else {        // Somebody's won the whole game!
            let winning_deck = match game_winner {
                Player1 => current_game.deck1,
                Player2 => current_game.deck2,
            };
            return (game_winner, winning_deck);
        }   
    }
}