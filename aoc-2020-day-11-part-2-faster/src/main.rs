
use std::io;
use std::time::Instant;
use std::cell::Cell;

fn main() {
    
    let start = Instant::now();
    let input_start = Instant::now();

    let mut seat_arrangement: Vec<isize> = Vec::new(); //-1 means no seat, otherwise it's the index in the seat array.

    let mut width = 0;

    let mut seat_index = -1isize;

    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read from stdin");
        let trimmed = input.trim();
        if trimmed.len() == 0 { break; }

        let mut row:Vec<isize> = trimmed.bytes().map(|b| { match b { b'.' => -1, b'L' => { seat_index += 1; seat_index } _ => panic!("Bad input"), }}).collect();

        if width == 0 { width = row.len() }

        seat_arrangement.append(&mut row);
    }

    let input_duration = input_start.elapsed();
    let setup_start = Instant::now();

    let num_seats = (seat_index + 1) as usize;

    let height = seat_arrangement.len()/width;  //integer division is okay because we assume all rows are equal

    let mut seat_visibility:Vec<Vec<&Cell<bool>>> = Vec::with_capacity(num_seats);
    let mut visible_counts:Vec<u32> = vec![0; num_seats];
    let seats:Vec<Cell<bool>> = vec![Cell::new(false); num_seats];  // not mutable, but has interior mutability

    for r in 0..height {
        let mut vis_row = (0..width).filter_map(|c| { 
            if seat_arrangement[r*width + c] == -1 { 
                None 
            } else { 
                Some(seen_seats(width as isize, r, c, &seat_arrangement, &seats)) 
            }} ).collect();
        seat_visibility.append(&mut vis_row);
    }

    
    let mut steps = 0;

    let setup_duration = setup_start.elapsed();
    let loop_start = Instant::now();

    loop {
        let changed = evolve(&seats, &seat_visibility, &mut visible_counts);
        if !changed { break; }
        steps += 1;
    }

    let loop_duration = loop_start.elapsed();

    let num_full_seats:usize = seats.into_iter().filter(|seat| seat.get()).count();
    println!("Stable after {} steps; number of filled seats:{}", steps, num_full_seats);

    let duration = start.elapsed();

    println!("Time elapsed:\nInput: {:?}\nSetup: {:?}\nLoop: {:?}\nTotal: {:?}", input_duration, setup_duration, loop_duration, duration);
}


fn seen_seats<'a>(width:isize, r:usize, c:usize, arr: &Vec<isize>, seats: &'a Vec<Cell<bool>>) -> Vec<&'a Cell<bool>> {
    const DIRS:[(isize, isize);8] = [(1,1),(1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1),(0,1)];
    let height = (arr.len() as isize)/width;
    DIRS.iter().filter_map(|(dr, dc)| {
            let mut rr = r as isize;
            let mut cc = c as isize;
            loop { 
                rr += dr;
                cc += dc;
                if (rr < 0) || (rr >= height) { return None; }  // if we're out of bounds, return None to drop the direction from the iter
                if (cc < 0) || (cc >= width) { return None; }
                if arr[(rr*width + cc) as usize] != -1 { break; }
            }
            Some(&seats[arr[(rr*width + cc) as usize] as usize])   // When we land on a seat, return reference to that seat's Cell
        }).collect()
}

fn ca_rule(current: &Cell<bool>, visible_count: u32) -> bool {
    let oldval = current.get();
    let newval = match oldval {
        false => visible_count == 0,  //empty seats with no neighbors are filled
        true => visible_count < 5,    //full seats that can see five or more full seats are emptied
    };
    current.set(newval);
    return newval != oldval;
}

//Return value is whether the array changed in the process
fn evolve(seats: &Vec<Cell<bool>>, seat_vis: &Vec<Vec<&Cell<bool>>>, visible_counts: &mut Vec<u32>) -> bool {
    let mut changed = false;

    // First, count visibles from each seat.
    for (count, vis) in visible_counts.iter_mut().zip(seat_vis.iter()) {
        *count = vis.iter().filter(|seat| seat.get() ).count() as u32;
    }

    // Then update the seat arrangement based on those numbers.
    for (seat, count) in seats.iter().zip(visible_counts.iter()) {
        let seat_changed = ca_rule(seat, *count);
        changed = changed || seat_changed;
    }

    changed
}