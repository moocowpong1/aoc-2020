use std::env;
use std::fs;
use std::error::Error;
use std::time::Instant;

fn main() -> Result<(), Box<dyn Error>> {
    let input: String;
    if let Some(arg1) = env::args().nth(1) {
        input = fs::read_to_string(arg1)?;
    } else {
        input = fs::read_to_string("puzzle_input.txt")?;
    }

    let part2_start = Instant::now();
    part2(input);
    let part2_duration = part2_start.elapsed();
    println!("Part 2 time: {:?}\n", part2_duration);

    Ok(())
}

#[derive(Clone, Copy, Debug)]
struct MemoryRegion {
    upper_bound: u128,
    lower_bound: u128,
}

#[derive(Clone, Copy, Debug)]
struct MemoryAllocation { 
    region: MemoryRegion,
    value: i128,
}

fn part2(input: String) {


    let mut ones_mask = 0u128; // mask of bits to set to 1 in the address
    let mut floating_mask = 0u128; // mask of bits which are "floating" and take all possible values
    // invariant: ones_mask & floating_mask = 0

    #[cfg(debug_assertions)]
    let mut num_bits = 0;

    let mut memory:Vec<MemoryAllocation> = Vec::new(); // sparse representation of memory

    for line in input.split('\n') {
        // guaranteed to be of the form /mask = [01X]{36}/ or /mem[\d+] = \d+/ exactly. We don't need regexes though.
        match line.as_bytes()[1] {  // check the second byte to distinguish mask vs mem
            b'a' => {
                // mask instruction
                ones_mask = 0;
                floating_mask = 0;
                #[cfg(debug_assertions)]
                if num_bits == 0 {
                    num_bits = line.bytes().count() - 7
                }

                for mask_byte in line.bytes().skip(7) { // trim off "mask = "
                    ones_mask <<= 1;
                    floating_mask <<= 1;
                    match mask_byte {
                        b'0' => (),
                        b'1' => ones_mask += 1,
                        b'X' => floating_mask += 1,
                        _ => panic!("Bad character"),
                    }
                }
            }
            b'e' => {
                //mem instruction
                let mut parts = line.split(']');
                let address = parts.next().expect("Bad input")[4..].parse::<u128>().expect("Parse error"); // Take the part before ']' and drop "mem[", then parse.
                let value = parts.next().expect("Bad input")[3..].parse::<i128>().expect("Parse error"); // Take the part after and drop " = ", then parse.

                let lower_bound = (address | ones_mask) & (!floating_mask);
                let upper_bound = (address | ones_mask) | floating_mask;

                let new_region = MemoryRegion { lower_bound, upper_bound };

                let mut intersections = Vec::new();
                
                for alloc in &memory {
                    if let Some(intersect) = intersection(alloc.region, new_region) {
                        intersections.push(MemoryAllocation { region: intersect, value: -alloc.value }); //Add a new memory region to cancel out the overlap if there is any.
                    }
                }

                memory.append(&mut intersections);

                memory.push(MemoryAllocation { region: new_region, value });
            }
            _ => panic!("Unexpected input"),
        }
    }

    // Now we compute the sum of all entries in memory. This is the sum over all allocations of the product of the size of the allocation and the value of the allocation.
    let mem_sum: i128 = memory.iter().map(|MemoryAllocation { region: reg, value: val } | {
        let extent = reg.lower_bound ^ reg.upper_bound; //find the indices they differ in; the size is 2 to the power of the number of bits that differ.
        (1 << (extent.count_ones())) * val
    }).sum();
    
    println!("Sum of all memory values: {}", mem_sum);

    #[cfg(debug_assertions)]
    {
        let mut region_sizes = vec![0u128; num_bits];
        for alloc in &memory {
            let extent = alloc.region.lower_bound ^ alloc.region.upper_bound;
            region_sizes[extent.count_ones() as usize] += 1;
        }

        let total_size:u128 = region_sizes.iter().enumerate().map(|(i, n)| {n * (1 << i)}).sum();
        let num_regions = memory.len();
        let avg_size = (total_size as f64) / (num_regions as f64);

        println!("Region counts by size: {:?}", region_sizes);
        println!("Number of memory regions: {}", num_regions);
        println!("Total size of all memory regions: {}", total_size);
        println!("Average size of a memory region: {}", avg_size);
    }
}

fn intersection(region1: MemoryRegion, region2: MemoryRegion) -> Option<MemoryRegion> {
    let lower_bound = region1.lower_bound | region2.lower_bound;
    let upper_bound = region1.upper_bound & region2.upper_bound;

    if upper_bound & lower_bound == lower_bound { // if upper_bound >= lower_bound bitwise
        Some(MemoryRegion { lower_bound, upper_bound })
    } else {
        None
    }
}