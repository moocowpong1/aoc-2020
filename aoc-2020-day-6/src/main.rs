use std::io;

fn main() {

    let mut total_and_answers = 0;
    let mut total_or_answers = 0;

    'outer: loop {  // loop over all groups

        // store question answers as flags in a bitfield
        let mut and_group_answers = u32::MAX; //neutral element under bitwise and is all 1's
        let mut or_group_answers = 0u32; //neutral element under bitwise or is all 0's

        loop { // loop over all individuals within a group

            let mut individual_answers = 0u32;

            let mut input = String::new();

            io::stdin().read_line(&mut input).expect("Failed to read from stdin");
            let trimmed = input.trim();

            if trimmed.len() == 0 { break; } // blank lines separate groups
            if trimmed.as_bytes()[0] == b'#' { break 'outer; } // we'll use a '#' to terminate the input

            for c in trimmed.bytes() {
                let question = c - b'a'; //convert the letter of the question to a number a=0 to z=25
                individual_answers |= 1 << question; //set the flag corresponding to the question
            }
            
            and_group_answers &= individual_answers;
            or_group_answers |= individual_answers;
        }

        //We want to count the number of set flags; I found an algorithm attributed to Brian Kernighan
        //based on the fact that for n positive, n & (n-1) is n with its least significant set bit unset.
        //So we can iterate this until we get to zero, and the number of loops is the number of set bits. 

        let mut num_and_answers = 0;

        while and_group_answers != 0 {
            num_and_answers += 1;
            and_group_answers &= and_group_answers - 1;
        }

        let mut num_or_answers = 0;

        while or_group_answers != 0 {
            num_or_answers += 1;
            or_group_answers &= or_group_answers - 1;
        }

        total_and_answers += num_and_answers;
        total_or_answers += num_or_answers;
    }

    println!("Total among groups of questions which every member answered: {}", total_and_answers);
    println!("Total among groups of questions which any member answered: {}", total_or_answers);
    
}
