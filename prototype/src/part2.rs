

pub fn part2(input: &str) -> usize {
    let mut nums: Vec<usize> = Vec::new();
    for line in input.split('\n') {
        nums.push(line.parse().expect("Parse error"));
    }
    nums.into_iter().product()
}