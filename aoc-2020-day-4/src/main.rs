#[macro_use]
extern crate lazy_static;

use std::io;
use regex::Regex;

#[derive(Debug, PartialEq, Eq, Default)]
struct Passport {
    birth_year: Option<String>,
    issue_year: Option<String>,
    expiration_year: Option<String>,
    height: Option<String>,
    hair_color: Option<String>,
    eye_color: Option<String>,
    passport_id: Option<String>,
    country_id: Option<String>,
}

fn has_reqd_fields(p: &Passport) -> bool {
    return Option::is_some(&p.birth_year) && Option::is_some(&p.issue_year) && Option::is_some(&p.expiration_year) 
            && Option::is_some(&p.height) && Option::is_some(&p.hair_color) && Option::is_some(&p.eye_color) && Option::is_some(&p.passport_id) // no need to check p.country_id
}

fn blank_passport() -> Passport {
    return Default::default();
}

fn read_passport() -> Passport {

    lazy_static! {
        static ref RE_BYR: Regex = Regex::new(r"byr:(\S+)").unwrap();
        static ref RE_IYR: Regex = Regex::new(r"iyr:(\S+)").unwrap();
        static ref RE_EYR: Regex = Regex::new(r"eyr:(\S+)").unwrap();
        static ref RE_HGT: Regex = Regex::new(r"hgt:(\S+)").unwrap();
        static ref RE_HCL: Regex = Regex::new(r"hcl:(\S+)").unwrap();
        static ref RE_ECL: Regex = Regex::new(r"ecl:(\S+)").unwrap();
        static ref RE_PID: Regex = Regex::new(r"pid:(\S+)").unwrap();
        static ref RE_CID: Regex = Regex::new(r"cid:(\S+)").unwrap();
    }

    let mut p = blank_passport();

    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read from stdin");
        let trimmed = input.trim();

        if trimmed.len() == 0 { break; }    // an empty line ends a passport entry

        p.birth_year        = p.birth_year.or_else(||       { RE_BYR.captures(trimmed).map(|caps| { String::from(&caps[1]) } ) });
        p.issue_year        = p.issue_year.or_else(||       { RE_IYR.captures(trimmed).map(|caps| { String::from(&caps[1]) } ) });
        p.expiration_year   = p.expiration_year.or_else(||  { RE_EYR.captures(trimmed).map(|caps| { String::from(&caps[1]) } ) });
        p.height            = p.height.or_else(||           { RE_HGT.captures(trimmed).map(|caps| { String::from(&caps[1]) } ) });
        p.hair_color        = p.hair_color.or_else(||       { RE_HCL.captures(trimmed).map(|caps| { String::from(&caps[1]) } ) }); //just store the string
        p.eye_color         = p.eye_color.or_else(||        { RE_ECL.captures(trimmed).map(|caps| { String::from(&caps[1]) } ) });
        p.passport_id       = p.passport_id.or_else(||      { RE_PID.captures(trimmed).map(|caps| { String::from(&caps[1]) } ) });
        p.country_id        = p.country_id.or_else(||       { RE_CID.captures(trimmed).map(|caps| { String::from(&caps[1]) } ) });
    }

    return p;
}

//Logically this wants to be a bool but an Option<()> is isomorphic and lets me use ? chaining
fn is_valid(p: &Passport) -> Option<()> {
    lazy_static! {
        static ref RE_BYR_FMT: Regex = Regex::new(r"^(\d{4})$").unwrap();
        static ref RE_IYR_FMT: Regex = Regex::new(r"^(\d{4})$").unwrap();
        static ref RE_EYR_FMT: Regex = Regex::new(r"^(\d{4})$").unwrap();
        static ref RE_HGT_FMT: Regex = Regex::new(r"^(\d+)(in|cm)$").unwrap();
        static ref RE_HCL_FMT: Regex = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
        static ref RE_ECL_FMT: Regex = Regex::new(r"^(amb|blu|brn|gry|grn|hzl|oth)$").unwrap();
        static ref RE_PID_FMT: Regex = Regex::new(r"^(\d{9})$").unwrap();
    }

    let birth_year_num = RE_BYR_FMT.captures(p.birth_year.as_ref()?.as_str())?[1].parse::<i32>().ok()?;
    if !((1920 <= birth_year_num) && (birth_year_num <= 2002)) { return None; }

    let issue_year_num = RE_IYR_FMT.captures(p.issue_year.as_ref()?.as_str())?[1].parse::<i32>().ok()?;
    if !((2010 <= issue_year_num) && (issue_year_num <= 2020)) { return None; }

    let expiration_year_num = RE_EYR_FMT.captures(p.expiration_year.as_ref()?.as_str())?[1].parse::<i32>().ok()?;
    if !((2020 <= expiration_year_num) && (expiration_year_num <= 2030)) { return None; }

    let height_caps = RE_HGT_FMT.captures(p.height.as_ref()?.as_str())?;
    let height_num = height_caps[1].parse::<i32>().ok()?;
    let inches = height_caps[2].as_bytes()[0] == b'i'; // we know height_caps[2] is either "in" or "cm"
    if (inches && !((59 <= height_num) && (height_num <= 76))) || (!inches && !((150 <= height_num) && (height_num <= 193))) { return None; }

    if !(RE_HCL_FMT.is_match(p.hair_color.as_ref()?.as_str())) { return None; }  // no further checks needed beyond the regex matching
    if !(RE_ECL_FMT.is_match(p.eye_color.as_ref()?.as_str())) { return None; }
    if !(RE_PID_FMT.is_match(p.passport_id.as_ref()?.as_str())) { return None; }

    //don't check country_id

    Some(())
}

fn main() {
    let mut num_reqd_fields:i32 = 0;
    let mut num_valid:i32 = 0;

    loop {
        let p = read_passport();
        
        if p == blank_passport() { break; }

        if has_reqd_fields(&p) {
            num_reqd_fields += 1;
        }

        if is_valid(&p).is_some() { num_valid += 1 }
    }

    println!("Number of passports with all required fields: {}", num_reqd_fields);
    println!("Number of valid passports: {}", num_valid);
}
