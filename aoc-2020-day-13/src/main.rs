use std::env;
use std::fs;
use std::error::Error;
use std::time::Instant;
use modinverse::modinverse;

fn main() -> Result<(), Box<dyn Error>> {
    let input: String;
    if let Some(arg1) = env::args().nth(1) {
        input = fs::read_to_string(arg1)?;
    } else {
        input = fs::read_to_string("puzzle_input.txt")?;
    }
    let input2 = input.clone();
    let part1_start = Instant::now();
    part1(input);
    let part1_duration = part1_start.elapsed();
    println!("Part 1 time: {:?}\n", part1_duration);

    let part2_start = Instant::now();
    part2(input2);
    let part2_duration = part2_start.elapsed();
    println!("Part 2 time: {:?}\n", part2_duration);

    Ok(())
}

fn part1(input: String) {
    let mut lines = input.split('\n');
    let current_time:i32 = lines.next().expect("Need a line").parse().expect("Parse error");
    let ids_iter = lines.next().expect("Need another line").split(',');
    let mut ids:Vec<i32> = Vec::new();
    for id in ids_iter {
        if id.as_bytes()[0] != b'x' {
            ids.push(id.parse().expect("Parse error"));
        }
    }

    let mut soonest_arrival_id = 0;
    let mut soonest_arrival_time = i32::MAX; //measured from now
    for id in ids.iter() {
        //absolute arrival time is just 0 mod id, so the relative arrival time is (0 - current_time) mod id.
        let arrival_time = (-current_time).rem_euclid(*id);
        if arrival_time < soonest_arrival_time {
            soonest_arrival_time = arrival_time;
            soonest_arrival_id = *id;
        }
    }

    println!("Next bus is #{} arriving in {} minutes; product = {}", soonest_arrival_id, soonest_arrival_time, soonest_arrival_id * soonest_arrival_time);
}

fn part2(input: String) {
    let mut bus_ids: Vec<i64> = Vec::new();
    let mut bus_ixs: Vec<i64> = Vec::new();   // the indexes in the bus list where these busses occur
    for (ix, id_input) in input.split('\n').nth(1).expect("Need two lines").split(',').enumerate() {    //ignore the first line, iterate over the parts of the second line
        if id_input.as_bytes()[0] != b'x' {
            bus_ids.push(id_input.parse().expect("Parse error"));
            bus_ixs.push(ix as i64);
        }
    }
    
    let product:i64 = bus_ids.iter().product(); //product of all the bus id's

    // We want to find the projections onto the factors that are used in the Chinese remainder theorem.
    // Each of these is the product of all factors *except* one, times the modular inverse of this number mod the omitted factor.
    // This makes it zero mod all factors except the omitted factor, for which its modulus is 1.
    // Fortunately there's a modinverse crate.
    // (We assume the inputs are relatively prime.)
    let modular_projections: Vec<i64> = bus_ids.iter().map(|id| {
        let prod_hat = product/id;          // product of all bus id's except this one
        let prod_hat_mod = prod_hat.rem_euclid(*id);
        prod_hat * modinverse(prod_hat_mod, *id).expect("No modular inverse; bus ids are not relatively prime")
    }).collect();

    // The number which has value n_i mod q_i is sum_i n_i * p_i, where p_i is the projection. Note that we need to take the result modulo the product to get the first example!
    // Also, the values we want as n_i's are actually the negatives of the ix's, so that ix minutes later the modulus is zero.
    let first_time: i64 = bus_ixs.iter().zip(modular_projections.iter()).map(|(ix, proj)| {
        -ix * proj
    }).sum::<i64>().rem_euclid(product);
    
    println!("First concurrence: {}", first_time);
}
