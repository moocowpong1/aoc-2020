
use std::fs;
use criterion::{criterion_group, criterion_main, Criterion};
use aoc_2020_day_24::{part1, part2};

fn benches(c: &mut Criterion) {
    let input = fs::read_to_string("puzzle_input.txt").unwrap();
    let mut group = c.benchmark_group("benches");
    group.bench_function("Part 1", |b| b.iter(|| { part1::part1(&input); }));
    group.bench_function("Part 2", |b| b.iter(|| { part2::part2(&input); }));
    group.finish();
}

criterion_group!(bench, benches);
criterion_main!(bench);