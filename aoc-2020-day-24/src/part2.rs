
use crate::part1;
use std::collections::BTreeMap;
use std::collections::BTreeSet;

pub fn part2(input: &str) -> usize {
    let mut tile_map = part1::read_tile_map(input);
    for _ in 0..100 {
        ca_step(&mut tile_map);
    }
    tile_map.values().filter(|tile| **tile).count()
}

fn ca_step(tile_map: &mut BTreeMap<(i32, i32), bool>) {
    let mut neighbor_map: BTreeMap<(i32, i32), u32> = BTreeMap::new();

    for ((pos_x, pos_y), _) in tile_map.iter().filter(|(_, tile)| **tile) {
        const DIRS: [(i32, i32);6] = [(1,0), (0,1), (-1,1), (-1, 0), (0, -1), (1,-1)];
        for (dx, dy) in &DIRS {
            let opt_neighbors = neighbor_map.get(&(pos_x + dx, pos_y + dy));
            match opt_neighbors {
                None => neighbor_map.insert((pos_x + dx, pos_y + dy), 1u32),
                Some(val) => {
                    let val = *val;
                    neighbor_map.insert((pos_x + dx, pos_y + dy), val + 1)
                }
            };
        }
    }

    let check_set: BTreeSet<(i32, i32)> = tile_map.keys().chain(neighbor_map.keys()).copied().collect();
    for (pos_x, pos_y) in check_set {
        let neighbors = neighbor_map.get(&(pos_x, pos_y)).unwrap_or(&0);
        let tile = tile_map.get(&(pos_x, pos_y)).unwrap_or(&false);
        let mut new_tile = *tile;
        match tile {
            true => if (*neighbors == 0) || (*neighbors > 2) { new_tile = false },
            false => if *neighbors == 2 { new_tile = true },
        }
        tile_map.insert((pos_x, pos_y), new_tile);
    }
}