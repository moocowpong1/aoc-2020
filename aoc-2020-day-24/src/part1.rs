
use std::collections::BTreeMap;

pub fn read_tile_map(input: &str) -> BTreeMap<(i32, i32), bool> {
    //true = black, false = white, not in map = false = white
    let mut tile_grid: BTreeMap<(i32, i32), bool> = BTreeMap::new();
    for line in input.split('\n') {
        let mut byte_iter = line.bytes();
        let mut pos_x:i32 = 0;  // coordinate in the east direction
        let mut pos_y:i32 = 0;  // coordinate in the north-east direction
        // north-west = north-east - east = (-1, 1); south-west = - north-west = (1, -1)
        loop {
            if let Some(byte) = byte_iter.next() {
                match byte {
                    b'e' => pos_x += 1,
                    b'w' => pos_x -= 1,
                    b'n' => {
                        match byte_iter.next() {
                            Some(b'e') => pos_y += 1,
                            Some(b'w') => { pos_x -= 1; pos_y += 1; },
                            _ => panic!("Incomplete line or bad second letter of direction."),
                        }
                    },
                    b's' => {
                        match byte_iter.next() {
                            Some(b'w') => pos_y -= 1,
                            Some(b'e') => { pos_x += 1; pos_y -= 1; },
                            _ => panic!("Incomplete line or bad second letter of direction."),
                        }
                    },
                    _ => panic!("Bad direction"),
                }
            } else {
                break;
            }
        }
        if let Some(tile) = tile_grid.get(&(pos_x, pos_y)) {
            let new_tile = !(*tile);
            tile_grid.insert((pos_x, pos_y), new_tile);
        } else {
            tile_grid.insert((pos_x, pos_y), true);
        }
    }
    tile_grid
}

pub fn part1(input: &str) -> usize {
    let tile_grid = read_tile_map(input);
    tile_grid.values().filter(|tile| **tile).count()
}