use std::io;
use std::cmp::max;
use std::cmp::min;

struct IntervalSumData {
    sum:u64,
    least:u64,
    greatest:u64,
}

fn main() {
    const PREAMBLE_LENGTH:usize = 25;

    let mut code:Vec<u64> = Vec::new();

    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read from stdin");
        let trimmed = input.trim();
        if trimmed.len() == 0 { break; } // break on an empty line
        code.push(trimmed.parse().expect("Failed to read number"));
    }

    let mut wrong_number = 0u64;

    for i in 0..(code.len() - PREAMBLE_LENGTH) {
        let target = code[i + PREAMBLE_LENGTH];
        let parts = &code[i..(i + PREAMBLE_LENGTH)];
        if !is_pair_sum(target, parts) {
            println!("{} is not a sum of a pair from {:?}", target, parts);
            wrong_number = target;
            break;
        }
    }

    // Now we iterate from the beginning of the list keeping a list of all running sums over intervals, looking for one which sums to wrong_number.
    // We abandon a running sum if it exceeds this number, as all numbers in 'code' are unsigned. 
    // We only need to know the least and greatest from the first interval with the correct sum. 

    let mut running_sums:Vec<IntervalSumData> = Vec::new();

    for i in 0..code.len() {
        running_sums = running_sums.iter().filter_map(|isd| { update_interval_sum_data(code[i], wrong_number, isd) }).collect();
        if let Some(isd) = running_sums.iter().find(|isd| { isd.sum == wrong_number } ) {
            println!("Found interval summing to {}. Least: {}, Greatest: {}, Least + Greatest: {}", wrong_number, isd.least, isd.greatest, isd.least + isd.greatest);
            break;
        }
        running_sums.push(IntervalSumData{ sum: code[i], least: code[i], greatest: code[i] });  // Add the current number to the list of running sums.
    }

}

fn is_pair_sum(target: u64, parts: &[u64]) -> bool {
    for i in 0..parts.len() {
        for j in (i+1)..parts.len() {
            if parts[i] + parts[j] == target { return true; }
        }
    }
    return false;
}

fn update_interval_sum_data(num: u64, goal: u64, current: &IntervalSumData) -> Option<IntervalSumData> {
    let new_isd = IntervalSumData { sum: current.sum + num, least: min(current.least, num), greatest: max(current.greatest, num) };
    return if new_isd.sum > goal { None } else { Some(new_isd) };
}