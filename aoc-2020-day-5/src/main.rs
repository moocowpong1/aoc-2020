use std::io;
use std::cmp::min;
use std::cmp::max;

fn main() {
    let mut max_seat_id:i16 = -1; // highest seat id seen so far
    let mut min_seat_id:i16 = 1025; // lowest seat id seen so far
    let mut running_xor:i16 = 0; // bitwise xor of all seats seen so far

    loop {
        let mut input = String::new();

        io::stdin().read_line(&mut input).expect("Failed to read from stdin");

        let code_string = input.trim().as_bytes();
        if code_string.len() < 10 { break; }    // terminate on a blank line, or one with not enough characters.

        let mut seat_id = 0i16;

        for i in 0..10 {
            let bit = match code_string[i] {
                b'F' => 0,
                b'L' => 0,
                b'B' => 1,
                b'R' => 1,
                x => panic!("Unexpected character: {}", x),
            };

            seat_id = (seat_id << 1) | bit;
        }
        
        min_seat_id = min(min_seat_id, seat_id);
        max_seat_id = max(max_seat_id, seat_id);
        running_xor ^= seat_id;
    }

    // The bitwise xor of all possible seat id's - all numbers 0 to 1023 - is zero, because each bit appears as a 0 and a 1 512 times each.
    // Thus the bitwise xor of all seat id's except one is just the missing seat id.
    // But we need to correct this by including all the ones missing at the front and back. 

    for id in 0..min_seat_id {      // 0 inclusive, min_seat_id exclusive
        running_xor ^= id;
    }

    for id in (max_seat_id + 1)..1024 { //(max_seat_id + 1) inclusive, 1024 exclusive
        running_xor ^= id;
    }

    // Now running_xor should be the missing seat id!
    println!("Min Seat ID: {}", min_seat_id);
    println!("Max Seat ID: {}", max_seat_id);
    println!("Missing Seat ID: {}", running_xor);

}