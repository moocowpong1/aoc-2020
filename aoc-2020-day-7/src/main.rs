#[macro_use]
extern crate lazy_static;

use std::io;
use std::collections::HashMap;
use std::collections::HashSet;
use regex::Regex;

// returns (outer bag name, HashMap<inner bag name -> number of bag>), maybe
fn read_rule() -> Option<(String, HashMap<String, u32>)> {
    lazy_static! {
        static ref RE_LINE: Regex = Regex::new(r"^([a-z\s]+) bags? contain ([a-z\s\d,]+)\.$").unwrap();    //match the whole line and capture the color of the outer bag
                                                                                                    //and the list of subbags
        static ref RE_INNER: Regex = Regex::new(r"(\d+) ([a-z\s]+) bags?").unwrap();    //matches a single (inner) bag
    }

    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Failed to read from stdin");
    let trimmed = input.trim();

    let line_caps = RE_LINE.captures(trimmed)?; //if there isn't a match for the line, return None
    let outer = String::from(&line_caps[1]);
    let mut inners = HashMap::new();

    for inner_caps in RE_INNER.captures_iter(&line_caps[2]) {
        let num = inner_caps[1].parse().expect("Read a confusing number");
        let color = String::from(&inner_caps[2]);
        inners.insert(color, num);
    }

    return Some((outer, inners));
}

fn count_possible_outer_bags(rule_map: &HashMap<String, HashMap<String, u32>>, target: &String) -> usize {
    let mut known_possible_outer_bags = HashSet::new();

    for (outer, inners) in rule_map.iter() {
        if inners.contains_key(target) {
            known_possible_outer_bags.insert(outer);    //seed set of possible outers with all bags directly containing the target ("shiny gold")
        }
    }

    let mut num_known = known_possible_outer_bags.len();

    loop {
        for (outer, inners) in rule_map.iter() {
            for inner in inners.keys() {
                if known_possible_outer_bags.contains(inner) {
                    known_possible_outer_bags.insert(outer);    //if any outer bag has any inner bag which is known to recursively contain the target,
                                                                //the outer bag is now known to recursively contain the target
                }
            }
        }

        let new_num_known = known_possible_outer_bags.len();

        if new_num_known == num_known {
            break;  //we didn't add anything this iteration, we must be done
        }

        num_known = new_num_known;
    }

    return num_known;
}

fn count_bags_inside(rule_map: &HashMap<String, HashMap<String, u32>>, target: &String) -> usize {

    let mut num_contained = 0;

    let target_contents = rule_map.get(target).expect("No rule for target bag");

    for (inner, n) in target_contents {
        num_contained += (*n as usize) * (1 + count_bags_inside(rule_map, inner));  // for each inner bag type, add the number of bags of that type times
                                                                                    // one (for the inner bag itself) plus the number contained in a bag of that type.
    }

    return num_contained;
}

fn main() {

    let mut rule_map = HashMap::new();

    while let Some((outer, inners)) = read_rule() {
        rule_map.insert(outer, inners);
    }

    let target = String::from("shiny gold");

    let num_possible_outers = count_possible_outer_bags(&rule_map, &target);
    let num_contained = count_bags_inside(&rule_map, &target);

    println!("Number of possible outer bags for a {} bag: {}", target, num_possible_outers);
    println!("Number of bags recursively contained in a {} bag: {}", target, num_contained);
}
