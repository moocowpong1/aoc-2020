
use std::collections::VecDeque;

const MAX_CUP: usize = 9;
const MIN_CUP: usize = 1;

pub fn parse_input(input: &str) -> VecDeque<usize> {
    let mut cups: VecDeque<usize> = VecDeque::new();
    for b in input.bytes() {
        cups.push_back((b - b'0') as usize);
    }
    cups
}

pub fn part1(input: &str) -> String {
    let mut cups = parse_input(input);
    for _ in 0..100 {
        game_step(&mut cups);
    }
    let (ix, _) = cups.iter().enumerate().find(|(_, cup)| **cup == 1).unwrap();
    cups.rotate_left(ix);
    String::from_utf8(cups.iter().skip(1).map(|cup| b'0'+(*cup as u8)).collect::<Vec<u8>>()).unwrap()
}

fn game_step(cups: &mut VecDeque<usize>) {
    let current_cup = cups[0];
    let moved_cups: Vec<usize> = cups.drain(1..=3).collect();
    let mut dest_cup = current_cup - 1;
    if dest_cup < MIN_CUP {
        dest_cup += MAX_CUP - MIN_CUP + 1;
    }
    let dest_ix;
    loop {
        if let Some((ix, _)) = cups.iter().enumerate().find(|(_,cup)| **cup == dest_cup) {
            dest_ix = ix;
            break;
        } else {
            dest_cup -= 1;
            if dest_cup < MIN_CUP {
                dest_cup += MAX_CUP - MIN_CUP + 1;
            }
        }
    }
    cups.insert(dest_ix + 1, moved_cups[0]);
    cups.insert(dest_ix + 2, moved_cups[1]);
    cups.insert(dest_ix + 3, moved_cups[2]);
    cups.rotate_left(1);
}