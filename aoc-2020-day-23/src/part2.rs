
const NUM_CUPS: usize = 1000000;
const NUM_MOVES: usize = 10000000;

struct CupRing {
    next_cup: Vec<usize>,
}

impl CupRing {
    // Initialize the cup ring with a vector of the first few 
    fn from_vec(cup_order: &Vec<usize>) -> CupRing {
        let mut next_cup = vec![0; cup_order.len()];
        for (c, next_c) in cup_order.iter().zip(cup_order.iter().skip(1)) {
            next_cup[*c] = *next_c;  
        }
        next_cup[cup_order[cup_order.len() - 1]] = cup_order[0];
        CupRing { next_cup }
    }

    fn iter(&self, starting_cup: usize) -> CupIterator {
        CupIterator { cup_ring: self, current_cup: starting_cup }
    }

    // Moves all cups starting after current_cup and ending with last_moved_cup inclusive to
    // just after dest_cup
    fn move_cups(&mut self, current_cup: usize, last_moved_cup: usize, dest_cup: usize) {
        let after_moved_cup = self.next_cup[last_moved_cup];
        let after_dest_cup = self.next_cup[dest_cup];
        let first_moved_cup = self.next_cup[current_cup];

        self.next_cup[dest_cup] = first_moved_cup;
        self.next_cup[last_moved_cup] = after_dest_cup;
        self.next_cup[current_cup] = after_moved_cup;
    }
}

struct CupIterator<'a> {
    cup_ring: &'a CupRing,
    current_cup: usize,
}

impl Iterator for CupIterator<'_> {
    type Item = usize;

    fn next(&mut self) -> Option<usize> {
        let res = self.current_cup;
        let next_cup = self.cup_ring.next_cup[self.current_cup];
        self.current_cup = next_cup;
        return Some(res);
    }
}

pub fn parse_input(input: &str) -> Vec<usize> {
    let mut cups_order: Vec<usize> = Vec::new();
    for b in input.bytes() {
        cups_order.push((b - b'1') as usize);   // use 0-indexed cups internally
    }
    cups_order
}

pub fn part2(input: &str) -> usize {
    let mut cups_order = parse_input(input);
    for cup in (cups_order.len())..NUM_CUPS
    {
        cups_order.push(cup);
    }
    let mut cup_ring = CupRing::from_vec(&cups_order);
    game(&mut cup_ring, cups_order[0]);
    cup_ring.iter(0).skip(1).take(2).map(|cup| cup + 1).product::<usize>()    // remember to account for 0-indexing
}

fn game(cup_ring: &mut CupRing, mut current_cup: usize) {
    let mut removed_cups: Vec<usize> = Vec::with_capacity(3);
    for _ in 1..=NUM_MOVES {
        let mut cup_iter = cup_ring.iter(current_cup).skip(1);
        removed_cups.extend((&mut cup_iter).take(3));
        
        let mut dest_cup = (current_cup + NUM_CUPS - 1) % NUM_CUPS;
        while removed_cups.contains(&dest_cup) {
            dest_cup = (dest_cup + NUM_CUPS - 1) % NUM_CUPS;
        }

        cup_ring.move_cups(current_cup, removed_cups[2], dest_cup);
        
        current_cup = cup_ring.next_cup[current_cup];

        removed_cups.resize(0,0);
    }
}