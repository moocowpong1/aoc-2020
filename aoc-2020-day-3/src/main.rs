use std::io;

fn main() {
    let mut map : Vec<Vec<bool>> = Vec::new();
    let mut width = 0usize;
    let mut height = 0usize;
    
    loop {
        let mut input = String::new();

        io::stdin().read_line(&mut input).expect("Failed to read");

        let line:Vec<bool> = input.trim().bytes().map(|b| { if b == b'#' { true } else { false } }).collect();
        if height == 0 { width = line.len(); }
        if line.len() > 0 {
            map.push(line);
            height += 1;
        }
        else { break; }
    }

    let directions = vec![(1,1), (3,1), (5,1), (7,1), (1,2)];

    let trees:Vec<usize> = directions.iter().map(|(xv, yv)| { check_trees(&map, width, height, *xv, *yv) }).collect();

    println!("Trees vec: {:?}, product: {}", trees, trees.iter().product::<usize>());
}

fn check_trees(map:&Vec<Vec<bool>>, width: usize, height: usize, xv: usize, yv: usize) -> usize
{
    let mut x = 0; let mut y = 0;
    let mut trees = 0;

    loop {
        if map[y][x] { trees += 1; }
        x = (x + xv) % width;
        y += yv;
        if y >= height { break; }
    }

    return trees;
}
