
use std::io;
use std::time::Instant;

#[derive(Clone, Debug, PartialEq, Eq)]
enum Seat {
    None,
    Empty,
    Full,
}

fn main() {
    
    let start = Instant::now();

    let mut seat_arrangement: Vec<Vec<Seat>> = Vec::new();

    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read from stdin");
        let trimmed = input.trim();
        if trimmed.len() == 0 { break; }

        let row:Vec<Seat> = trimmed.chars().map(as_seat).collect();

        seat_arrangement.push(row);
    }


    let height = seat_arrangement.len();
    let width = seat_arrangement[0].len();

    let mut seat_visibility:Vec<Vec<Vec<(usize, usize)>>> = Vec::new();

    for r in 0..height {
        seat_visibility.push((0..width).map(|c| { seen_seats(r, c, &seat_arrangement) }).collect());
    }

    let mut steps = 0;

    loop {
        let (new_arrangement, changed) = evolve(seat_arrangement, &seat_visibility);
        seat_arrangement = new_arrangement;
        if !changed { break; }
        steps += 1;
    }

    let num_full_seats:usize = seat_arrangement.into_iter().map(|row| { row.into_iter().filter(|seat| { *seat == Seat::Full }).count() }).sum();

    println!("Stable after {} steps; number of filled seats:{}", steps, num_full_seats);

    let duration = start.elapsed();

    println!("Time elapsed: {:?}", duration);
}

/* Printing utility functions
fn print_arrangement(arr:&Vec<Vec<Seat>>) {
    for row in arr {
        println!("{}", String::from_utf8(row.into_iter().map(seat_to_byte).collect()).expect("Display error"));
    }
    println!("");
}

fn seat_to_byte(seat: &Seat) -> u8 {
    match seat {
        Seat::None => b'.',
        Seat::Empty => b'L',
        Seat::Full => b'#',
    }
}
*/

fn seen_seats(r:usize, c:usize, arr: &Vec<Vec<Seat>>) -> Vec<(usize, usize)> {
    const DIRS:[(isize, isize);8] = [(1,1),(1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1),(0,1)];
    let height = arr.len() as isize;
    let width = arr[0].len() as isize;
    DIRS.iter().filter_map(|(dr, dc)| {
            let mut rr = r as isize;
            let mut cc = c as isize;
            loop { 
                rr += dr;
                cc += dc;
                if (rr < 0) || (rr >= height) { return None; }  // if we're out of bounds, return None to drop the direction from the iter
                if (cc < 0) || (cc >= width) { return None; }
                if arr[rr as usize][cc as usize] != Seat::None { break; }
            }
            Some((rr as usize,cc as usize))   // otherwise, we must have landed on a seat. 
        }).collect()
}

fn as_seat(ch:char) -> Seat {
    match ch {
        '.' => Seat::None,
        'L' => Seat::Empty,
        '#' => Seat::Full,
        _ => panic!("Failed to read seat"),
    }
}

//nbhd is always 3x3 but fixed sized arrays don't seem to work well for expressing that
fn ca_rule(current: &Seat, visible: Vec<&Seat>) -> Seat {
    let full_visible = visible.into_iter().filter(|s| { **s == Seat::Full }).count();
    match current {
        Seat::None => Seat::None,
        Seat::Empty => if full_visible == 0 { Seat::Full } else { Seat::Empty },    //empty seats with no neighbors are filled
        Seat::Full => if full_visible >= 5 { Seat::Empty } else { Seat::Full },     //full seats that can see five or more full seats are emptied
    }
}

//Return value is the new array, and whether it's changed.
fn evolve(arr: Vec<Vec<Seat>>, seat_vis: &Vec<Vec<Vec<(usize, usize)>>>) -> (Vec<Vec<Seat>>, bool) {
    let height = arr.len();
    let width = arr[0].len();
    let mut changed = false;

    let mut result = Vec::new();
    result.reserve(height); //to avoid multiple resizes as we fill it

    for r in 0..height {   //iterate over rows
        let mut result_row = Vec::new();
        result_row.reserve(width);
        for c in 0..width {
            let current_seat = &arr[r][c];
            let visible = seat_vis[r][c].iter().map(|(rr, cc)| { &arr[*rr][*cc] }).collect();
            let new_seat = ca_rule(current_seat, visible);
            changed = changed || (&new_seat != current_seat);
            result_row.push(new_seat);
        }
        result.push(result_row);
    }

    (result, changed)
}