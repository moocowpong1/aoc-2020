
use fxhash::FxBuildHasher;
use std::collections::HashMap;
use modinverse::modinverse;

pub const INITIAL_SUBJECT: i64 = 7;
pub const CRYPTO_PRIME: i64 = 20201227;
pub const PRIME_SQRT: i64 = 4495; // = ceiling of sqrt(20201227)

pub fn part1(input: &str) -> i64 {
    let mut lines = input.lines();
    let card_key: i64 = lines.next().unwrap().parse().unwrap();
    let door_key: i64 = lines.next().unwrap().parse().unwrap();

    // The transformation in question here is
    // subject_number => subject_number^loop_size mod p
    // So card_key = 7^card_loop_size mod p,
    // and door_key = 7^door_loop_size mod p.
    // We want to compute 7^(card_loop_size * door_loop_size) mod p.

    // Use the baby-step giant-step algorithm. We want to find x such that
    // alpha^x = beta; write x = i m + j where m = PRIME_SQRT and
    // 0 <= i, j < m
    // This means alpha^(i m + j) = beta, so alpha^j = beta alpha^(-i m), and
    // alpha^j = beta (alpha^(-m))^i

    // First, compute alpha^j for all j, and store the results in a hashtable or similar.
    // Then, compute the RHS one i at a time and efficiently check for the j that solves it.
    // Here alpha = INITIAL_SUBJECT and beta is either card_key or door_key

    let mut alpha_js: HashMap::<i64, i64, FxBuildHasher> = HashMap::with_capacity_and_hasher(PRIME_SQRT as usize, Default::default());

    let mut alpha_j = 1;
    for j in 0..PRIME_SQRT {
        alpha_js.insert(alpha_j, j);
        alpha_j *= INITIAL_SUBJECT;
        alpha_j %= CRYPTO_PRIME;
    }

    // At this point alpha_j = alpha^m, so invert it to compute alpha^(-m).

    let alpha_minv = modinverse(alpha_j, CRYPTO_PRIME).unwrap();
    let mut gamma = card_key;
    let mut i = 0;
    let solution_j;
    loop {
        if let Some(j) = alpha_js.get(&gamma) {
            solution_j = j;
            break;
        }
        i += 1;
        gamma = (gamma * alpha_minv) % CRYPTO_PRIME;
    }

    let mut card_loop_size = i * PRIME_SQRT + solution_j;

    // Now calculate door_key^card_loop_size mod CRYPTO_PRIME
    let mut result = 1;
    let mut door_key_power = door_key;
    while card_loop_size != 0 {
        if card_loop_size % 2 == 1 {
            result = (result * door_key_power) % CRYPTO_PRIME;
            card_loop_size -= 1;
        }
        card_loop_size >>= 1;
        door_key_power = (door_key_power * door_key_power) % CRYPTO_PRIME;
    }
    
    result

}