
use std::env;
use std::fs;
use std::error::Error;
use aoc_2020_day_25::part1;

fn main() -> Result<(), Box<dyn Error>> {
    let input = fs::read_to_string(env::args().nth(1).unwrap_or(String::from("puzzle_input.txt")))?;

    println!("Part 1:");
    println!("{}", part1::part1(&input));

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::{fs, part1};

    #[test]
    fn test_part_1() {
        let input = fs::read_to_string("puzzle_input.txt").unwrap();
        assert!(3803729 == part1::part1(&input));
    }

}