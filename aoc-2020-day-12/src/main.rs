use std::env;
use std::fs;
use std::error::Error;
use std::time::Instant;

fn main() -> Result<(), Box<dyn Error>> {
    let input: String;
    if let Some(arg1) = env::args().skip(1).next() {
        input = fs::read_to_string(arg1)?;
    } else {
        input = fs::read_to_string("puzzle_input.txt")?;
    }
    let input2 = input.clone();
    let part1_start = Instant::now();
    part1(input);
    let part1_duration = part1_start.elapsed();
    println!("Part 1 time: {:?}\n", part1_duration);

    let part2_start = Instant::now();
    part2(input2);
    let part2_duration = part2_start.elapsed();
    println!("Part 2 time: {:?}\n", part2_duration);

    Ok(())
}



#[derive(Clone, Copy, PartialEq, Eq)]
enum InstType {
    N,
    E,
    S,
    W,
    L,
    R,
    F,
}

struct Instruction {
    inst_type: InstType,
    val: i32,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Facing {
    North,
    East,
    South,
    West,
}

#[derive(Debug)]
struct ShipPosition {
    north: i32,
    east: i32,
    facing: Facing,
}

fn part1(input: String) {
    let mut instructions: Vec<Instruction> = Vec::new();
    for line in input.split('\n') {
        let inst_type = match line.as_bytes()[0] {
            b'N' => InstType::N,
            b'E' => InstType::E,
            b'S' => InstType::S,
            b'W' => InstType::W,
            b'L' => InstType::L,
            b'R' => InstType::R,
            b'F' => InstType::F,
            _ => panic!("Bad instruction"),
        };
        let val = line[1..].parse().expect("Parse error");
        instructions.push(Instruction { inst_type, val })
    }

    let mut ship_pos = ShipPosition { north: 0, east: 0, facing: Facing::East };
    instructions.iter().map(|inst| { move_ship(&mut ship_pos, inst) }).for_each(drop);     // really a fold, but the accumulator is the mutable ship_pos.

    println!("Final ship position:{:?}\nManhattan distance from origin: {}", ship_pos, ship_pos.north.abs() + ship_pos.east.abs());
}

fn move_ship(pos: &mut ShipPosition, inst: &Instruction) {
    match inst.inst_type {
        InstType::N => pos.north += inst.val,
        InstType::E => pos.east += inst.val,
        InstType::S => pos.north -= inst.val,
        InstType::W => pos.east -= inst.val,
        InstType::R => rotate_facing(&mut pos.facing, inst.val),
        InstType::L => rotate_facing(&mut pos.facing, -inst.val),
        InstType::F => match pos.facing {
            Facing::North => pos.north += inst.val,
            Facing::East => pos.east += inst.val,
            Facing::South => pos.north -= inst.val,
            Facing::West => pos.east -= inst.val,
        }
    }
}

//Positive angle is clockwise, negative is counter-clockwise. Angle which are not multiples of 90 degrees are UB.
fn rotate_facing(facing: &mut Facing, angle: i32) {
    let n = match *facing {
        Facing::North => 0,
        Facing::East => 1,
        Facing::South => 2,
        Facing::West => 3
    };
    match (n + angle/90).rem_euclid(4) {
        0 => *facing = Facing::North,
        1 => *facing = Facing::East,
        2 => *facing = Facing::South,
        3 => *facing = Facing::West,
        _ => panic!("Somthing's wrong with rem_euclid..."),
    }
}



//relative to the ship
#[derive(Debug)]
struct Waypoint {
    north: i32,
    east: i32,
}

#[derive(Debug)]
struct ShipState {
    north: i32,
    east: i32,
    waypoint: Waypoint,
}

fn part2(input: String) {
    let mut instructions: Vec<Instruction> = Vec::new();
    for line in input.split('\n') {
        let inst_type = match line.as_bytes()[0] {
            b'N' => InstType::N,
            b'E' => InstType::E,
            b'S' => InstType::S,
            b'W' => InstType::W,
            b'L' => InstType::L,
            b'R' => InstType::R,
            b'F' => InstType::F,
            _ => panic!("Bad instruction"),
        };
        let val = line[1..].parse().expect("Parse error");
        instructions.push(Instruction { inst_type, val })
    }

    let mut ship = ShipState { north: 0, east: 0, waypoint: Waypoint { north: 1, east: 10 } };
    instructions.iter().for_each(|inst| { move_ship2(&mut ship, inst) });

    println!("Final ship state: {:?}\nManhattan distance: {}", ship, ship.north.abs() + ship.east.abs());
}

fn rotate_waypoint_right(waypoint: &mut Waypoint) {
    let temp = waypoint.east;
    waypoint.east = waypoint.north;
    waypoint.north = -temp;
}

fn rotate_waypoint(waypoint: &mut Waypoint, angle: i32) {
    let times = (angle/90).rem_euclid(4);   //number of times to rotate right
    for _ in 0..times {
        rotate_waypoint_right(waypoint);
    }
}

fn move_ship2(ship: &mut ShipState, inst: &Instruction) {
    match inst.inst_type {
        InstType::N => ship.waypoint.north += inst.val,
        InstType::E => ship.waypoint.east += inst.val,
        InstType::S => ship.waypoint.north -= inst.val,
        InstType::W => ship.waypoint.east -= inst.val,
        InstType::R => rotate_waypoint(&mut ship.waypoint, inst.val),
        InstType::L => rotate_waypoint(&mut ship.waypoint, -inst.val),
        InstType::F => {
            ship.north += ship.waypoint.north * inst.val;
            ship.east += ship.waypoint.east * inst.val;
        }
    }
}