#[macro_use]
extern crate lazy_static;

use std::fs;
use regex::Regex;

fn main() {
    let input = fs::read_to_string("puzzle_input.txt").expect("Failed to read file");
    let mut answers1:Vec<u64> = Vec::new();
    let mut answers2:Vec<u64> = Vec::new();
    for line in input.split('\n') {
        answers1.push(eval1(String::from(line)).unwrap());
        answers2.push(eval2(String::from(line)).unwrap());
    }

    println!("Part 1 answer: {}", answers1.iter().sum::<u64>());
    println!("Part 2 answer: {}", answers2.iter().sum::<u64>());
}

fn eval1(mut line: String) -> Option<u64> {
    lazy_static! {
        static ref INNER_PAREN_RE:Regex = Regex::new(r"\(([^\(\)]*)\)").unwrap();
    }
    
    while let Some(caps) = INNER_PAREN_RE.captures(&line) {
        let full_range = caps.get(0)?.range();
        let inner_range = caps.get(1)?.range();
        let val = eval_flat1(&line[inner_range])?;
        line.replace_range(full_range, &val.to_string());
    }

    eval_flat1(&line)
}

fn eval_flat1(expr: &str) -> Option<u64> {
    let mut tokens = expr.split_whitespace();

    let mut num = tokens.next()?.parse::<u64>().ok()?;

    while let Some(op) = tokens.next() {
        let operand = tokens.next()?.parse::<u64>().ok()?;
        match op {
            "+" => num += operand,
            "*" => num *= operand,
            _ => return None,
        }
    } 

    Some(num)
}

fn eval2(mut line: String) -> Option<u64> {
    lazy_static! {
        static ref INNER_PAREN_RE:Regex = Regex::new(r"\(([^\(\)]*)\)").unwrap();
    }
    
    while let Some(caps) = INNER_PAREN_RE.captures(&line) {
        let full_range = caps.get(0)?.range();
        let inner_range = caps.get(1)?.range();
        let val = eval_flat2(&line[inner_range])?;
        line.replace_range(full_range, &val.to_string());
    }

    eval_flat2(&line)
}

fn eval_flat2(expr: &str) -> Option<u64> {
    let mut tokens = expr.split_whitespace();

    let mut sum = tokens.next()?.parse::<u64>().ok()?;
    let mut product = 1;

    while let Some(op) = tokens.next() {
        let operand = tokens.next()?.parse::<u64>().ok()?;
        match op {
            "+" => sum += operand,
            "*" => { product *= sum; sum = operand; },
            _ => return None,
        }
    }

    product *= sum;

    Some(product)
}
