use std::io;
use regex::Regex;

fn main() {
    
    let re = Regex::new(r"^(\d+)-(\d+) ([a-z]): ([a-z]+)$").unwrap();
            //matches #-# c: pass
            //where #'s are nonnegative integers, c is a character, and pass is a sequence of lowercase characters

    let mut valids_a = 0u32;
    let mut valids_b = 0u32;

    loop {
        let mut input_text = String::new();
        io::stdin().read_line(&mut input_text).expect("Failed to read");
        let trimmed = input_text.trim();
        
        match re.captures_iter(trimmed).next() {
            Some(cap) => {
                let n1 = cap[1].parse::<usize>().expect("read a confusing number");
                let n2 = cap[2].parse::<usize>().expect("read a confusing number");
                let c = &cap[3];
                let s = &cap[4];
                if is_valid_a(n1, n2, c, s) { valids_a += 1; }
                if is_valid_b(n1-1, n2-1, c, s) { valids_b += 1; }
            }
            None => break,
        }
    }

    println!("Valids according to old policy: {}", valids_a);
    println!("Valids according to new policy: {}", valids_b);
}

fn is_valid_a(mincount: usize, maxcount: usize, c: &str, s: &str) -> bool {
    let n = s.matches(c).count();
    return (mincount <= n) && (n <= maxcount);
}

fn is_valid_b(i1: usize, i2: usize, c: &str, s: &str) -> bool {
    let b1 = s.as_bytes()[i1] == c.as_bytes()[0];
    let b2 = s.as_bytes()[i2] == c.as_bytes()[0];
    return b1 ^ b2;
}